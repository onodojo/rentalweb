package interceptors;

import models.entity.Group;
import models.entity.User;
import models.service.GroupService;
import models.service.UserService;
import models.session.UserInfo;
import play.libs.F.Option;
import play.libs.F.Promise;
import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.Http.Session;
import play.mvc.SimpleResult;
import views.html.index;

public class LoginCheckInterceptor extends Action<LoginCheck>{

	@Override
	public Promise<SimpleResult> call(Context ctx) throws Throwable {
		
		Session session = ctx.session();
		
		String loginurl = "/login?token=" + session.get("token");
		
		if(session.get("userInfo") == null){
			return Promise.<SimpleResult>pure(redirect(loginurl));
		}
		
		Option<UserInfo> sessiondata = UserInfo.getInstance(session.get("userInfo"));
		long userId = 0;
		long groupId = 0;
		
		// ユーザ情報の検索
		userId = sessiondata.get().id;
		UserService userService = new UserService();
		Option<User> userInfo = userService.findById(userId);
		
		if(!(userInfo.isDefined())){
			return Promise.<SimpleResult>pure(redirect(loginurl));
		}
		
		// グループ情報の検索
		groupId = sessiondata.get().groupId;
		GroupService groupService = new GroupService();
		Option<Group> groupInfo = groupService.findById(groupId);

		//　検索結果の判定
		if(!(groupInfo.isDefined())){
			return Promise.<SimpleResult>pure(redirect(loginurl));
		}
		
//		Option<UserInfo> oUserInfo = UserInfo.getInstance(session.get("userInfo"));
//		
//		if(oUserInfo.isEmpty()){
//			return Promise.<SimpleResult>pure(redirect(controllers.routes.LoginController.index()));
//		}

		return delegate.call(ctx);
	}

}
