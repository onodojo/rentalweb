package interceptors;

import models.session.UserInfo;
import play.libs.F.Option;
import play.libs.F.Promise;
import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.Http.Session;
import play.mvc.SimpleResult;

public class AdminCheckInterceptor extends Action<LoginCheck>{

	@Override
	public Promise<SimpleResult> call(Context ctx) throws Throwable {
		
		Session session = ctx.session();
		Option<UserInfo> sessiondata = UserInfo.getInstance(session.get("userInfo"));
		UserInfo userInfo = sessiondata.get();
		
		if(!userInfo.isAdmin()){
			return Promise.<SimpleResult>pure(badRequest("ページにアクセスできません。"));
		}

		return delegate.call(ctx);
	}

}
