package models.form;

import play.data.validation.Constraints;

/**
 * Created by h-ono on 2016/05/02.
 */
public class ApplyForm {
    @Constraints.Required(message = "ISBN番号を入力してください")
    @Constraints.Pattern(value = "^[0-9]+$",message = "ISBN番号は半角数値で入力してください")
    public String isbn;

    @Constraints.Required(message = "書籍名を入力してください")
    public String bookName;

    @Constraints.Required
    public String bookType;

    public String url;

    @Constraints.Required(message = "理由を入力してください")
    public String reason;

    @Constraints.Required(message = "金額を入力してください")
    @Constraints.Pattern(value = "^[0-9]+$",message = "金額は半角数値で入力してください")
    public String price;

    @Constraints.Required(message = "数量を入力してください")
    @Constraints.Pattern(value = "^[0-9]+$",message = "数量は半角数値で入力してください")
    public String volume;
    
    @Constraints.Required
    public String buyer;
}
