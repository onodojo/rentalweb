package models.form;

public class BookForm {
    /**
     * ISBN
     */
    public String isbn;

    /**
     * 書籍名
     */
    public String name;

    /**
     *　書籍情報URL
     */
    public String url;

    /**
     * 出版社名
     */
    public String publisher;

    /**
     * 著者
     */
    public String author;
    
    /**
     * おすすめ
     */
    public String recommend;

    /**
     * 書籍の種類
     */
    public String bookType;

    /**
     * 場所/機器ID
     */
    public Long shelfId;

    /**
     * 数量
     */
    public Long amount;

    /**
     * 削除対象承認済み書籍ID
     */
    public Long applyDeleteId;
}
