package models.form;

import java.util.Map;

import models.entity.User;
import models.service.UserService;
import models.session.UserInfo;
import play.data.validation.Constraints.Required;
import play.data.validation.Constraints.ValidateWith;
import play.data.validation.Constraints.Validator;
import play.libs.F.Option;
import play.libs.F.Tuple;
import play.mvc.Http;
import play.mvc.Http.Request;
import play.mvc.Http.Session;

public class UserForm {
	/**
     * ユーザID
     */
    public Long id;

    /**
     * ユーザ名
     */
	@Required(message="氏名を入力してください。")
    public String name;

    /**
     * パスワード
     */
	@Required(message="パスワードを入力してください。")
    public String password;

    /**
     * メールアドレス
     */
	@Required(message="メールアドレスを入力してください")
	@ValidateWith(value=MailCheck.class, message="メールアドレスが重複しています。")
    public String mailaddress;

    /**
     * 権限
     */
	@Required(message="権限を選択してください")
    public String authority;

	public static class MailCheck extends Validator <String>{

		public boolean isValid(String s){

			Session session = Http.Context.current().session();
	    	//Form<UserForm> f = form(UserForm.class).bindFromRequest();
//			String sss = Http.Context.current().request().username();

			Request request =  Http.Context.current().request();
			Map<String,String[]> formData = request.body().asFormUrlEncoded();
			String url = request.method();
			//String[] data = formData.get("name");
			//System.out.println(data);
			//String[] mail = formData.get("mailaddress");
			//String aaa = mail.toString();
			//System.out.println(mail[0]);

			// セッション情報の取得
			String userinfoJson =session.get("userInfo");

			Option<UserInfo> u = UserInfo.getInstance(userinfoJson);
		//	Long userId = u.get().id;
			UserService userService = new UserService();
			//編集の場合
			if(url=="PUT"){
//							Long userI = request.
							//form内容を取得
							int pos = request.path().lastIndexOf("/");
							Long user = Long.valueOf(request.path().substring(pos+1));
							//変更前のメールアドレスを取得
							Option<User> selectedUser = userService.findById(user);
							String selectedUserMailAddress = selectedUser.get().mailaddress;
							//入力内容のメールアドレスを取得
					    	String[] inputMailAddress = formData.get("mailaddress");
					    	String input = inputMailAddress[0];
							//Long selectedId = selectedID.get().id;
							//編集対象の値に変更がない場合メールアドレスチェックを行わない
//							if(user == userId ){
							if(selectedUserMailAddress.equals(input)){
//							if(input=="ddd" ){
								return true;
							}
			}
			// セッションからグループIDを取得
			Option<User> userMailResult = userService.findByMailAddress(s,u.get().groupId);
			//メールアドレス重複チェック
			if(userMailResult.isDefined()){
				return false;
//		      	ObjectMapper mapper = new ObjectMapper();
//		      	String formError =   f.error(mail).message();
//		      	formError="重複するメールアドレスが存在します";
	      }
			return true;
		}

		@Override
		public Tuple<String, Object[]> getErrorMessageKey() {
			return  new Tuple<String,Object[]>(toString(),new String[]{});
		}
	}
}


