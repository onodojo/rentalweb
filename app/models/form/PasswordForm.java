package models.form;

import play.data.validation.Constraints.Required;

public class PasswordForm {
	
	@Required(message="入力してください。")
	public String oldPass;
	
	@Required(message="入力してください。")
	public String newPass;
	
	@Required(message="入力してください。")
	public String newPassConfirm;
}


