package models.form;

import play.data.validation.Constraints.Required;

public class SignupForm {
	
	@Required(message="グループ名を入力してください")
	public String groupName;

    public String empNo;

    /**
     * ユーザ名
     */
    @Required(message="氏名を入力してください")
    public String name;

    /**
     * ログインID
     */
    public String loginId;

    /**
     * パスワード
     */
    @Required(message="パスワード入力してください")    
    public String password;
    
    /**
     * パスワード再確認
     */
    public String passwordConfirm;

    /**
     * メールアドレス
     */
    @Required(message="メールアドレスを入力してください。")
    public String mailaddress;
    
    public boolean invalidPassword(){
    	return !this.password.equals(this.passwordConfirm);
    }
}
