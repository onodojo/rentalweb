package models.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name = "m_shelf")
public class Shelf extends Model {
	private static final long serialVersionUID = 1L;

	public Shelf() {
		super();
	}
	
	public Shelf(Long id) {
		super();
		this.id = id;
	}

	@Id
    @GeneratedValue
	public Long id;
	
	public String name;
	
	public Integer kind;
	
	@ManyToOne
	public Group group;
	
	public static Finder<Long,Shelf> find = new Finder<Long,Shelf>(Long.class,Shelf.class);

}
