package models.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "m_user")
public class User extends Model {

	private static final long serialVersionUID = 1L;
	
    /**
     * ユーザID
     */
    @Id
    @GeneratedValue
    public Long id;

//    /**
//     * 社員番号
//     */
//    public String empNo;

    /**
     * ユーザ名
     */
    public String name;

//    /**
//     * ログインID
//     */
//    public String loginId;

    /**
     * パスワード
     */
    public String password;

    /**
     * メールアドレス
     */
    public String mailaddress;

    /**
     * 権限
     */
    public String authority;

    /**
     * グループ
     */
    @ManyToOne
    public Group group;
    
    public User(){
    	
    }
    
    public User(Long id){
    	this.id = id;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    public List<Rental> rentalList;

    public static Finder<Long, User> find = new Finder<Long, User>(Long.class, User.class);

}
