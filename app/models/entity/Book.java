package models.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import models.form.BookForm;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name = "m_book")
public class Book extends Model {
	
	/** デフォルトコンストラクタ */
	public Book() {
		super();
	}
	
	/**
	 * コンストラクタ
	 * フォーム入力値から値を設定
	 * @param form
	 */
	public Book(BookForm form) {
		super();
		isbn = form.isbn;
		name = form.name;
		url = form.url;
		publisher = form.publisher;
		author = form.author;
		// bookType = form.bookType;
		recommend = form.recommend;
		if (form.shelfId == 0) {
			shelf = null;
		} else {
			shelf = new Shelf(form.shelfId);
		}
	}

	/**
     * 書籍ID
     */
    @Id
    @GeneratedValue
    public Long id;
    
    public String isbn;
    
    public String name;
    
    public String url;
    
    public String publisher;
    
    public String author;
    
//    public String bookType;
    
    public String recommend;
    
    public Date createDate;
    
    @ManyToOne
    public Shelf shelf;
    
    @ManyToOne
    public Group group;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "book")
    @OrderBy("id desc")
    public List<Rental> rentalList;
    
    public boolean isRentaled(){
    	if(rentalList == null || rentalList.isEmpty() || rentalList.get(0).returnDate != null){
    		return false;
    	}
    	return true;
    }
    
    public static Finder<Long, Book> find = new Finder<Long, Book>(Long.class, Book.class);
}
