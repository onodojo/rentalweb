package models.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

/**
 * t_applyのエンティティ
 * @author h-ono
 *
 */
@Entity
@Table(name = "t_apply")
public class Apply extends Model {
    
	private static final long serialVersionUID = 1L;
	
	/**
	 * 申請ID
	 */
	@Id
    @GeneratedValue
    public Long id;
	
	/**
	 * 申請No
	 */
	public String requestNo;
	
	/**
	 * ISBN
	 */
	@NotNull
	public String isbn;
	
	/**
	 * 書籍名
	 */
	@NotNull
	public String bookName;
	
	/**
	 * 申請日
	 */
	@NotNull
	public Date applyDate;
	
	/**
	 * 理由
	 */
	@NotNull
	public String reason;
	
	/**
	 * 購入者
	 */
	public String buyer;
	
	/**
	 * 数量
	 */
	@NotNull
	public int volume;
	
	/**
	 * 価格
	 */
	@NotNull
	public int price;
	
	/**
	 * 書籍種類
	 */
	@NotNull
	public String bookType;
    
	/**
	 * 申請者
	 */
	@ManyToOne
    public User user;
	
	/**
	 * グループ
	 */
	@ManyToOne
	public Group group;
	
	/**
	 * 承認者
	 */
	@ManyToOne
	public User authorizer;
	
	/**
	 * 承認日
	 */
    public Date authorizeDate;
    
    /**
     * 承認区分
     */
    public String applyState;
    
    /**
     * 購入先URL
     */
    public String url;
	
    public static Finder<Long, Apply> find = new Finder<Long, Apply>(Long.class, Apply.class);
}
