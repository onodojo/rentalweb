package models.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import play.db.ebean.Model;

/**
 * 月報Webのユーザマスタエンティティ
 */
@Entity
@Table(name = "m_user")
public class GeppouUser extends Model {
	
	private static final long serialVersionUID = 1L;
	
	/** ユーザID */
	public Long userId;
	
	/** ユーザ名 */
	public String userName;
	
	/** 社員番号 */
	public String empCode;
	
	/** 姓 */
	public String sei;
	
	/** 名 */
	public String mei;
	
	/** 退職フラグ */
	public Long retirementFlg;
}
