package models.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "m_group")
public class Group extends Model {

    private static final long serialVersionUID = 1L;

    public Group(){

    }

    public Group(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    public Long id;

    /**
     * グループ名
     */
    public String name;

    /**
     * お知らせ
     */
    public String anounce;
    
    public String token;
    
    public int newArrivalSpan;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group")
    public List<User> userList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group")
    @OrderBy("id asc")
    public List<Shelf> shelfList;

    public static Finder<Long,Group> find = new Finder<Long,Group>(Long.class,Group.class);

}
