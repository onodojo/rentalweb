package models.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "t_rental")
public class Rental extends Model {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    public Long id;
    
    public Date rentalDate;
    
    public Date returnPlanDate;
    
    public Date returnDate;
    
    @ManyToOne
    public Book book;
    
    @ManyToOne
    public User user;
    
    @ManyToOne
    public Group group;
    
    public static Finder<Long,Rental> find = new Finder<Long,Rental>(Long.class,Rental.class);
}
