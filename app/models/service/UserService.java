package models.service;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import models.entity.GeppouUser;
import models.entity.Group;
import models.entity.User;
import play.libs.F;
import play.libs.F.Option;
import utils.CommonConstants;
import utils.ConfUtil;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.EbeanServerFactory;
import com.avaje.ebean.config.DataSourceConfig;
import com.avaje.ebean.config.ServerConfig;
import com.orangesignal.csv.Csv;
import com.orangesignal.csv.CsvConfig;
import com.orangesignal.csv.handlers.ColumnPositionMappingBeanListHandler;

public class UserService implements ModelService<User> {

	public static UserService use() {
		return new UserService();
	}

	/**
	 * メールアドレスとパスワードに一致するユーザを取得し、返却します。
	 * 
	 * @param mailaddress
	 *            メールアドレス
	 * @param password
	 *            パスワード
	 * @return ユーザ情報
	 */
	public Option<User> find(String mailaddress, String password) {
		User user = User.find.where().eq("mailaddress", mailaddress)
				.eq("password", password).findUnique();
		Option<User> optionUser = apply(user);
		return optionUser;
	}

	/**
	 * グループID,メールアドレス,パスワードに一致するユーザを取得し、返却します。
	 * 
	 * @param mailaddress
	 * @param password
	 * @param groupId
	 * @return
	 */
	public Option<User> find(String mailaddress, String password, Long groupId) {
		User user = User.find.where().eq("mailaddress", mailaddress)
				.eq("password", password).eq("group_id", groupId).findUnique();
		return apply(user);
	}

	/*
	 * (非 Javadoc) IDに一致するユーザ情報の取得
	 * 
	 * @see models.service.ModelService#findById(java.lang.Long)
	 */
	@Override
	public Option<User> findById(Long id) {
		User user = User.find.byId(id);
		Option<User> optionUser = apply(user);
		return optionUser;
	}

	@Override
	public Option<List<User>> findByGroupId(Long groupId) {

		// グループに紐付くユーザ一覧を取得
		List<User> userList = User.find.where().eq("group_id", groupId)
				.findList();

		return apply(userList);
	}

	/**
	 * グループID,メールアドレスに一致するユーザを取得し、返却します。
	 * 
	 * @param groupId
	 * @param mailaddress
	 * @return
	 */
	public Option<User> findByGroupIdAndMailaddress(Long groupId,
			String mailaddress) {
		// グループに紐付くユーザ一覧を取得
		List<User> userList = User.find.where().eq("group_id", groupId)
				.eq("mailaddress", mailaddress).findList();
		if (userList.isEmpty()) {
			return apply(null);
		}

		return apply(userList.get(0));
	}

	/**
	 * 引数で指定したグループ内の管理者を返却します。
	 * 
	 * @param groupId
	 *            グループID
	 * @return 管理者
	 */
	public Option<List<User>> findByGroupId4Admin(Long groupId) {
		// グループに紐付くユーザ一覧を取得
		List<User> userList = User.find.where().eq("group_id", groupId)
				.eq("authority", "0").findList();

		return apply(userList);
	}

	/*
	 * (非 Javadoc) ユーザ情報登録
	 * 
	 * @see models.service.ModelService#save(play.db.ebean.Model)
	 */
	@Override
	public Option<User> save(User entry) {
		Option<User> optionUser = apply(entry);
		if (optionUser.isDefined()) {
			entry.save();
		}
		return optionUser;
	}

	/*
	 * (非 Javadoc) ユーザ情報更新
	 * 
	 * @see models.service.ModelService#update(play.db.ebean.Model)
	 */
	@Override
	public Option<User> update(User entry) {
		Option<User> optionUser = apply(entry);
		if (optionUser.isDefined()) {
			entry.update();
		}
		return optionUser;
	}

	/**
	 * 対象IDのユーザ情報の削除を行う
	 *
	 * @param id
	 * @return
	 */
	public Option<User> deleteById(Long id) {
		Option<User> optionUser = findById(id);
		if (optionUser.isDefined()) {
			optionUser.get().delete();
		}
		return optionUser;
	}

	/**
	 * CSVアップロード
	 * 
	 * @param file
	 * @param groupId
	 * @throws IOException
	 */
	public void updateByCsv(File file, Long groupId) throws IOException {
		CsvConfig cfg = new CsvConfig();
		cfg.setSkipLines(1);
		cfg.setIgnoreEmptyLines(true);
		cfg.setQuoteDisabled(false);
		cfg.setEscapeDisabled(false);
		List<User> userList = Csv.load(file, "Shift-JIS", cfg,
				new ColumnPositionMappingBeanListHandler<User>(User.class)
						.addColumn(0, "name").addColumn(1, "mailaddress")
						.addColumn(2, "password").addColumn(3, "authority"));

		for (User user : userList) {
			Option<User> optUser = findByGroupIdAndMailaddress(groupId,
					user.mailaddress);

			// Option<User> optUser = update(user);
			if (!optUser.isDefined()) {
				user.group = new Group(groupId);
				user.save();
			}
		}
	}

	/**
	 * CSVダウンロード
	 * 
	 * @param file
	 * @param groupId
	 * @throws IOException
	 */
	public void outputByCsv(File file, Long groupId) throws IOException {

		CsvConfig cfg = new CsvConfig();

		// グループに紐付くユーザ一覧を取得
		List<User> userList = User.find.where().eq("group_id", groupId)
				.findList();

		Csv.save(userList, file, "Shift-JIS", cfg,
				new ColumnPositionMappingBeanListHandler<User>(User.class)
				// .addColumn(0, "empNo")
						.addColumn(0, "name").addColumn(1, "mailaddress")
						// .addColumn(3, "loginId")
						.addColumn(2, "password").addColumn(3, "authority"));
	}

	public void deleteAll(Long groupId) {
		// Ebean.createSqlUpdate("DELETE FROM m_user WHERE group_id = :groupId").setParameter("groupId",
		// groupId).execute();
	}

	/**
	 * グループID,メールアドレスに一致するユーザを取得し、返却します。
	 * 
	 * @param mailaddress
	 * @param groupId
	 * @return
	 */
	public Option<User> findByMailAddress(String mailaddress, Long groupId) {
		User users = User.find.where().eq("mailaddress", mailaddress)
				.eq("group_id", groupId.longValue()).findUnique();
		Option<User> optionUser = apply(users);
		// Option<User> optionUser = apply(users.get(0));
		return optionUser;
	}

	/**
	 * 月報WebのDBからユーザ情報を取得し、貸出管理のDBのユーザ情報を更新します。
	 * 
	 * @param groupId
	 */
	public void collectUploadByGeppouDb(Long groupId) {
		// 月報WebのDBからユーザ情報取得
		List<GeppouUser> geppouUserList = findUserByGeppouDb();

		// 貸出管理のDBのユーザ情報更新
		for (GeppouUser geppouUser : geppouUserList) {
			// Logger.info(geppouUser.empCode);
			// 既存の登録情報があるか確認
			Option<User> defaultUser = findByGroupIdAndMailaddress(groupId,
					geppouUser.userName
							+ CommonConstants.DCOM_MAILADDRESS_DOMAIN);
			if (defaultUser.isDefined()) {
				// 退職者は削除
				if (geppouUser.retirementFlg == CommonConstants.GEPPOU_USER_RETIREMENT) {
					defaultUser.get().delete();
				} else {
					// 名前のみ更新
					User user = defaultUser.get();
					user.name = geppouUser.sei + geppouUser.mei;
					update(user);
				}
			} else {
				// 退職者は登録しない
				if (geppouUser.retirementFlg == CommonConstants.GEPPOU_USER_RETIREMENT) {
					continue;
				}
				// 新規登録(利用者権限)
				User user = new User();
				user.group = new Group(groupId);
				user.authority = CommonConstants.USER_AUTHORITY;
				user.mailaddress = geppouUser.userName
						+ CommonConstants.DCOM_MAILADDRESS_DOMAIN;
				user.name = geppouUser.sei + geppouUser.mei;
				user.password = geppouUser.empCode;
				save(user);
			}
		}
	}

	/**
	 * 月報WebのDBから全ユーザ情報を取得します。
	 * 
	 * @return
	 */
	private List<GeppouUser> findUserByGeppouDb() {
		// 月報WebのDB接続情報設定
		final DataSourceConfig dsConfig = new DataSourceConfig() {
			{
				setDriver(ConfUtil.get("db.default.driver"));
				setUrl(ConfUtil.get("dcomgw.url"));
				setUsername(ConfUtil.get("dcomgw.user"));
				setPassword(ConfUtil.get("dcomgw.password"));
			}
		};

		// 月報WebのDBへの接続
		EbeanServer server = EbeanServerFactory.create(new ServerConfig() {
			{
				setName(ConfUtil.get("dcomgw.db"));
				setDataSourceConfig(dsConfig);
				setClasses(Arrays.<Class<?>> asList(GeppouUser.class));
				setRegister(false);
				setDdlRun(false);
				setDdlGenerate(false);
				setDefaultServer(false);
			}
		});

		return server.find(GeppouUser.class).findList();
	}

	/**
	 * Optionラップ
	 *
	 * @param value
	 *            ラップ対象
	 * @return
	 */
	private <A> F.Option<A> apply(A value) {
		if (value != null) {
			return F.Option.Some(value);
		} else {
			return F.Option.None();
		}
	}
}
