package models.service;

import java.util.List;

import play.db.ebean.Model;
import play.libs.F.Option;

public interface ModelService <T extends Model>{
	/**
	 * IDによる検索を行い、検索結果を返却します。
	 * @param id ID
	 * @return Option
	 */
	public Option<T> findById(Long id);
	/**
	 * グループIDによる検索を行い、検索結果を返却します。
	 * @param groupId グループID
	 * @return Option
	 */
	public Option<List<T>> findByGroupId(Long groupId);
	/**
	 * 引数で渡されたデータをDBに登録します。
	 * @param entry 登録データ
	 * @return Option
	 */
	public Option<T> save(T entry);
	/**
	 * 引数で渡されたデータでDBを更新します。
	 * @param entry 更新データ
	 * @return Option
	 */
	public Option<T> update(T entry);

	/**
	 * 引数で渡されたデータをDBから削除します。
	 * @param entry 削除データ
	 * @return Option
	 */
	//public Option<T> delete(T entry);
}
