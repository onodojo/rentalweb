package models.service;

import java.util.List;

import com.avaje.ebean.ExpressionList;
import models.entity.Apply;
import play.libs.F;
import play.libs.F.Option;

public class ApplyService implements ModelService<Apply>{

	public static ApplyService use(){
		return new ApplyService();
	}

	@Override
	public Option<Apply> findById(Long id) {
		Apply apply = Apply.find.byId(id);
		Option<Apply> optionApply = apply(apply);
		return optionApply;
	}

	@Override
	public Option<List<Apply>> findByGroupId(Long groupId) {

		// グループに紐付くユーザ一覧を取得
    	List<Apply> applyList = Apply.find.where().eq("group_id", groupId).findList();
    	return apply(applyList);
	}

	public Option<List<Apply>> findByFilter(Long groupId,String filter){
		ExpressionList<Apply> exp = Apply.find.where().eq("group_id", groupId);

		if("1".equals(filter) || "2".equals(filter)){
			// 承認済み
			exp.eq("apply_state",filter);
		}else{
			exp.eq("apply_state","0");
		}

		return apply(exp.findList());
	}

	public Option<List<Apply>> findByUserId(Long userId,String filter){
		ExpressionList<Apply> exp = Apply.find.where().eq("user_id", userId);

		if("1".equals(filter) || "2".equals(filter)){
			// 承認済み
			exp.eq("apply_state",filter);
		}else{
			exp.eq("apply_state","0");
		}

		return apply(exp.findList());
	}

	@Override
	public Option<Apply> save(Apply entry) {
		Option<Apply> optionApply = apply(entry);
		optionApply.get().save();
		return optionApply;
	}

	@Override
	public Option<Apply> update(Apply entry) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	public Option<Apply> delete(Apply apply) {
		Option<Apply> optionApply = apply(apply);
		if(optionApply.isDefined()){
			optionApply.get().delete();
		}
		return optionApply;
    }
	
	
	public Option<Apply> deleteById(Long id) {
		Option<Apply> optionApply = findById(id);
		if(optionApply.isDefined()){
			optionApply.get().delete();
		}
		return optionApply;
    }

	/**
	 * Optionラップ
	 *
	 * @param value ラップ対象
	 * @return
	 */
	private <A> F.Option<A> apply(A value) {
        if(value != null) {
            return F.Option.Some(value);
        } else {
            return F.Option.None();
        }
    }
}
