package models.service;

import java.util.Date;
import java.util.List;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;

import models.entity.Book;
import models.entity.Rental;
import play.libs.F;
import play.libs.F.Option;

public class RentalService implements ModelService<Rental>{

	public static RentalService use(){
		return new RentalService();
	}

	@Override
	public Option<Rental> findById(Long id) {
		Rental rental = Rental.find.byId(id.longValue());
		return apply(rental);
	}

	@Override
	public Option<List<Rental>> findByGroupId(Long groupId) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	public Option<List<Rental>> findByBookId(Long bookId) {
		List<Rental> rentalList = Rental.find.where().eq("book_id", bookId).findList();
		return apply(rentalList);
	}
	
	public Option<List<Rental>> findExcessByGroupId(Long groupId){
		List<Rental> rentalList = Rental.find.where()
												.eq("group_id", groupId)
												.lt("return_plan_date", new Date())
												.eq("return_date",null)
											.findList();
		return apply(rentalList);
	}
	
	public List<SqlRow> findRanking(Long groupId){
		return Ebean.createSqlQuery("SELECT b.name as bookName,COUNT(r.id) as rentalCount FROM t_rental r JOIN m_book b ON b.id = r.book_id WHERE r.group_id = :groupId GROUP BY b.name ORDER BY rentalCount DESC").setParameter("groupId", groupId).findList();
	}

	@Override
	public Option<Rental> save(Rental entry) {
		Option<Rental> optionRental = apply(entry);
		optionRental.get().save();
		return optionRental;
	}

	@Override
	public Option<Rental> update(Rental entry) {
		Option<Rental> optionRental = apply(entry);
		if(optionRental.isDefined()){
			entry.update();
		}
		return optionRental;
	}

	/**
	 * Optionラップ
	 *
	 * @param value ラップ対象
	 * @return
	 */
	private <A> F.Option<A> apply(A value) {
        if(value != null) {
            return F.Option.Some(value);
        } else {
            return F.Option.None();
        }
    }
}
