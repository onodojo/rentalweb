package models.service;

import java.util.List;

import play.libs.F;
import play.libs.F.Option;
import utils.OptionUtil;
import models.entity.Book;
import models.entity.Group;
import models.entity.Shelf;
import models.entity.User;

public class ShelfService implements ModelService<Shelf>{
	
	public static ShelfService use(){
		return new ShelfService();
	}

	@Override
	public Option<Shelf> findById(Long id) {
		Shelf shelf = Shelf.find.byId(id.longValue());
		return apply(shelf);
	}

	@Override
	public Option<List<Shelf>> findByGroupId(Long groupId) {
		// グループに紐付くユーザ一覧を取得
    	List<Shelf> shelfList = Shelf.find.where().eq("group_id", groupId).findList();

    	return apply(shelfList);
	}

	@Override
	public Option<Shelf> save(Shelf entry) {
		Option<Shelf> idOps = OptionUtil.apply(entry);
		if (idOps.isDefined()) {
			entry.save();
			if (OptionUtil.apply(entry.id).isDefined()) {
				return OptionUtil.apply(entry);
			}
		}
		return Option.None();
	}

	@Override
	public Option<Shelf> update(Shelf entry) {
		Option<Shelf> option = apply(entry);
		if(option.isDefined()){
			entry.update();
		}
		return option;
	}
	
	public Option<Shelf> delete(Long id){
		Option<Shelf> optionShelf = findById(id);
		if(optionShelf.isDefined()){
			optionShelf.get().delete();
		}
		return optionShelf;

	}

	/**
	 * Optionラップ
	 *
	 * @param value ラップ対象
	 * @return
	 */
	private <A> F.Option<A> apply(A value) {
        if(value != null) {
            return F.Option.Some(value);
        } else {
            return F.Option.None();
        }
    }
}
