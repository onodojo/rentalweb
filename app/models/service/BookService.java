package models.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.entity.Book;
import models.entity.Group;
import models.entity.Shelf;
import models.entity.User;
import play.Logger;
import play.libs.F;
import play.libs.F.Option;
import utils.CommonConstants;

import com.orangesignal.csv.Csv;
import com.orangesignal.csv.CsvConfig;
import com.orangesignal.csv.handlers.ColumnNameMapListHandler;
import com.orangesignal.csv.handlers.ColumnPositionMappingBeanListHandler;

public class BookService implements ModelService<Book> {

	// CSV出力時の固定ヘッダー
	private final String BOOK_CSV_ISBN = "isbn";
	private final String BOOK_CSV_NAME = "name";
	private final String BOOK_CSV_URL = "url";
	private final String BOOK_CSV_PUBLISHER = "publisher";
	private final String BOOK_CSV_AUTHOR = "author";
	private final String BOOK_CSV_BOOK_TYPE = "book_type";
	private final String BOOK_CSV_RECOMMEND = "recommend";
	private final int BOOK_CSV_FIX_HEADER = 7;

	/** 書籍情報CSVの[場所/機器]判定フラグ */
	private final String BOOK_CSV_SHELF_VALUE = "1";

	public static BookService use() {
		return new BookService();
	}

	@Override
	public Option<Book> findById(Long id) {
		Book book = Book.find.byId(id);
		Option<Book> optionBook = apply(book);
		return optionBook;
	}

	@Override
	public Option<List<Book>> findByGroupId(Long groupId) {
		// グループに紐付くユーザ一覧を取得
		List<Book> bookList = Book.find.where().eq("group_id", groupId)
				.findList();

		return apply(bookList);
	}

	/**
	 * 新着の書籍を取得、返却します。
	 * 
	 * @param groupId
	 *            グループID
	 * @param limitDate
	 *            新着期限
	 * @return 書籍情報
	 */
	public Option<List<Book>> findNewArrival(Long groupId, Date limitDate) {
		List<Book> bookList = Book.find.where().eq("group_id", groupId)
				.ge("create_date", limitDate).orderBy("create_date desc")
				.findList();
		return apply(bookList);
	}

	@Override
	public Option<Book> save(Book entry) {
		Option<Book> optionBook = apply(entry);
		if (optionBook.isDefined()) {
			entry.save();
		}
		return optionBook;
	}

	@Override
	public Option<Book> update(Book entry) {
		Option<Book> optionBook = apply(entry);
		if (optionBook.isDefined()) {
			entry.update();
		}
		return optionBook;
	}

	public Option<Book> delete(Book book) {
		Option<Book> optionBook = apply(book);
		if (optionBook.isDefined()) {
			optionBook.get().delete();
		}
		return optionBook;
	}

	/**
	 * 書籍データcsvアップロード
	 *
	 * @param file
	 * @param groupId
	 *            グループID
	 * @throws IOException
	 */
	public void updateByCsv(File file, Long groupId) throws Exception {
		CsvConfig cfg = new CsvConfig();
		cfg.setSkipLines(1);
		cfg.setIgnoreEmptyLines(true);
		cfg.setQuoteDisabled(false);
		cfg.setEscapeDisabled(false);

		// CSV読込用ヘッダーの作成
		ColumnNameMapListHandler cmlh = new ColumnNameMapListHandler();
		Map<Long, String> shelfMap = new HashMap<>();
		setCsvHeader(groupId, cmlh, shelfMap);

		// CSVデータの読み込み
		List<Map<String, String>> inputList = Csv.load(file,
				CommonConstants.CHARSET_SHIFT_JIS, cfg, cmlh);
		for (Map<String, String> data : inputList) {
			Book book = new Book();
			book.isbn = data.get(BOOK_CSV_ISBN);
			book.name = data.get(BOOK_CSV_NAME);
			book.url = data.get(BOOK_CSV_URL);
			book.publisher = data.get(BOOK_CSV_PUBLISHER);
			book.author = data.get(BOOK_CSV_AUTHOR);
//			book.bookType = data.get(BOOK_CSV_BOOK_TYPE);
			book.recommend = data.get(BOOK_CSV_RECOMMEND);
			for (String header : shelfMap.values()) {
				if (BOOK_CSV_SHELF_VALUE.equals(data.get(header))) {
					book.shelf = new Shelf(Long.valueOf(header
							.split(CommonConstants.SEP_HYPHEN)[0]));
					break;
				}
			}
			book.group = new Group(groupId);
			save(book);
		}
	}

	/**
	 * 書籍データcsvダウンロード
	 * 
	 * @param csvOs
	 *            出力データ書込み用ストリーム
	 * @param groupId
	 *            グループID
	 * @throws IOException
	 */
	public void outputByCsv(OutputStream csvOs, Long groupId) throws Exception {
		CsvConfig cfg = new CsvConfig();
		cfg.setQuoteDisabled(false);
		cfg.setEscapeDisabled(false);

		// CSVヘッダーの作成
		ColumnNameMapListHandler cmlh = new ColumnNameMapListHandler();
		Map<Long, String> shelfMap = new HashMap<>();
		setCsvHeader(groupId, cmlh, shelfMap);

		// CSV出力する書籍情報の取得
		List<Book> bookList = Book.find.where().eq("group_id", groupId)
				.findList();
		// CSV出力用のデータ整形
		List<Map<String, String>> outputList = new ArrayList<>();
		for (Book book : bookList) {
			Map<String, String> outputMap = new HashMap<>();
			outputMap.put(BOOK_CSV_ISBN, book.isbn);
			outputMap.put(BOOK_CSV_NAME, book.name);
			outputMap.put(BOOK_CSV_URL, book.url);
			outputMap.put(BOOK_CSV_PUBLISHER, book.publisher);
			outputMap.put(BOOK_CSV_AUTHOR, book.author);
//			outputMap.put(BOOK_CSV_BOOK_TYPE, book.bookType);
			outputMap.put(BOOK_CSV_RECOMMEND, book.recommend);
			if (shelfMap.containsKey(book.shelf.id)) {
				outputMap
						.put(shelfMap.get(book.shelf.id), BOOK_CSV_SHELF_VALUE);
			}
			outputList.add(outputMap);
		}
		Csv.save(outputList, csvOs, CommonConstants.CHARSET_SHIFT_JIS, cfg,
				cmlh);
	}

	/**
	 * Optionラップ
	 *
	 * @param value
	 *            ラップ対象
	 * @return
	 */
	private <A> F.Option<A> apply(A value) {
		if (value != null) {
			return F.Option.Some(value);
		} else {
			return F.Option.None();
		}
	}

	/**
	 * 書籍データcsvのヘッダー情報を設定する
	 * 
	 * @param groupId
	 *            グループID
	 * @param cmlh
	 *            ヘッダーオブジェクト
	 * @param shelfMap
	 *            ヘッダーに設定した場所/機器情報
	 */
	private void setCsvHeader(final Long groupId,
			ColumnNameMapListHandler cmlh, Map<Long, String> shelfMap) {
		cmlh.addColumn(BOOK_CSV_ISBN).addColumn(BOOK_CSV_NAME)
				.addColumn(BOOK_CSV_URL).addColumn(BOOK_CSV_PUBLISHER)
				.addColumn(BOOK_CSV_AUTHOR).addColumn(BOOK_CSV_BOOK_TYPE)
				.addColumn(BOOK_CSV_RECOMMEND);
		// 場所/機器情報の取得
		List<Shelf> shelfList = ShelfService.use().findByGroupId(groupId).get();
		// 動的ヘッダー作成
		for (Shelf shelf : shelfList) {
			String header = shelf.id + CommonConstants.SEP_HYPHEN + shelf.name;
			shelfMap.put(shelf.id, header);
			cmlh.addColumn(header);
		}
	}
}