package models.service;

import java.util.List;

import models.entity.Group;
import models.entity.Shelf;
import play.libs.F;
import play.libs.F.Option;
import utils.OptionUtil;

public class GroupService implements ModelService<Group>{

	public static GroupService use(){
		return new GroupService();
	}

	@Override
	public Option<Group> findById(Long id) {
		// TODO 自動生成されたメソッド・スタブ
		Group groupList = Group.find.where().eq("id", id).findUnique();
		return apply(groupList);

	}

	@Override
	public Option<List<Group>> findByGroupId(Long groupId) {
		// グループに紐付くユーザ一覧を取得
    	List<Group> groupList = Group.find.where().eq("id", groupId).findList();

    	return apply(groupList);
	}
	
	public Option<Group> findByToken(String token){
		Group group = Group.find.where().eq("token", token).findUnique();
		return apply(group);
	}

	@Override
	public Option<Group> save(Group entry) {
		Option<Group> idOps = OptionUtil.apply(entry);
		if (idOps.isDefined()) {
			entry.save();
			if (OptionUtil.apply(entry.id).isDefined()) {
				return OptionUtil.apply(entry);
			}
		}
		return Option.None();
	}

	@Override
	public Option<Group> update(Group entry) {
		Option<Group> option = apply(entry);
		if(option.isDefined()){
			entry.update();
		}
		return option;
	}

//	@Override
	public Option<Group> deleteByGroupId(Long groupId){
		Option<Group> optionGroup = findById(groupId);
		if(optionGroup.isDefined()){
			optionGroup.get().delete();
		}
		return optionGroup;

	}

	/**
	 * Optionラップ
	 *
	 * @param value ラップ対象
	 * @return
	 */
	private <A> F.Option<A> apply(A value) {
        if(value != null) {
            return F.Option.Some(value);
        } else {
            return F.Option.None();
        }
    }
}
