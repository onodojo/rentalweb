package models.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import play.data.validation.ValidationError;

public class ResultError {
	public int result;
	public List<Map<Object,Object>> errors;
	
	//private Map<String, List<ValidationError>> vErrors;
	
	public ResultError(Map<String, List<ValidationError>> vErrors){
		Iterator<Entry<String, List<ValidationError>>> ite = vErrors.entrySet().iterator();
		
		result = -1;
		errors = new ArrayList<>();
		while(ite.hasNext()){
			Entry<String, List<ValidationError>> entry = ite.next();
			Map<Object,Object> map = new HashMap<>();
			map.put("name", entry.getKey());
			map.put("message", entry.getValue().get(0).message());
			
			errors.add(map);
		}
		
	}
}
