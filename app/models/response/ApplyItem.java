package models.response;


public class ApplyItem {

	/**
	 * 申請ID
	 */
    public Long id;

	/**
	 * リクエストNo
	 */
    public String requestNo;
    
	/**
	 * ISBN
	 */
	public String isbn;
	
	/**
	 * 書籍名
	 */
	public String bookName;

	/**
	 * 申請日
	 */
	public String applyDate;

	/**
	 * 理由
	 */
	public String reason;

	/**
	 * 数量
	 */
	public int volume;

	/**
	 * 価格
	 */
	public String price;

	/**
	 * 書籍種類
	 */
	public String bookType;

	/**
	 * 申請者
	 */
	public String applyUser;
	
	/**
	 * 購入先URL
	 */
	public String url;

}
