package models.response;

public class ShelfItem {

	/**
	 * id
	 */
	public Long id;
	
	/**
	 * 場所/機器名
	 */
	public String name;
	
	/**
	 * 種類
	 */
	public Integer kind;
	
}
