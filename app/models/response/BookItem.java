package models.response;

import models.entity.Book;
import models.entity.Rental;
import utils.DateUtil;

public class BookItem {

	/** コンストラクタ */
	public BookItem(){
	}

	/**
	 * コンストラクタ
	 * 書籍エンティティから値を設定
	 */
	public BookItem(Book book){
		id = book.id;
		isbn = book.isbn;
		url = book.url;
		name = book.name;
		publisher = book.publisher;
		author = book.author;
		if (book.shelf != null) {
			shelfId = book.shelf.id;
			shelfName = book.shelf.name;
		}
//		bookType = book.bookType;
		recommend = book.recommend;
		// 貸出中書籍の場合は貸出情報を設定
		if (book.isRentaled()) {
			Rental rental = book.rentalList.get(0);
			rentalUserId = rental.user.id;
			rentalDate = DateUtil.paseDateToString(rental.rentalDate);
			returnPlanDate = DateUtil.paseDateToString(rental.returnPlanDate);
			userName = rental.user.name;
			rentalId = rental.id;
		}
	}

	/** id */
	public Long id;

	/** ISBN */
	public String isbn;

	/** 機器・場所 */
	public String place;

	/** 書籍情報URL */
	public String url;

	/** 書籍名 */
	public String name;

	/** 出版社 */
	public String publisher;

	/** 著者 */
	public String author;

	/** 貸出日 */
	public String rentalDate;

	/** 返却日 */
	public String returnPlanDate;

	/** 利用者ID */
	public Long rentalUserId;

	/** 利用者 */
	public String userName;

	/** 機器・場所ID */
	public Long shelfId;

	/** 機器・場所名 */
	public String shelfName;

	/** 書籍種別 */
	public String bookType;

	/** おすすめ */
	public String recommend;

	/** 貸出ID */
	public Long rentalId;
}
