package models.session;


import java.io.IOException;

import javax.persistence.ManyToOne;

import models.entity.Group;
import models.entity.User;
import play.libs.F.Option;
import utils.CommonConstants;
import utils.OptionUtil;




//import java.beans.Transient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * セッションに格納するユーザ情報
 * @author h-ono
 *
 */
public class UserInfo {
	/**
	 * ユーザ名
	 */
	public String name;
	/**
	 * グループID
	 */
	public Long groupId;
	/**
	 * ユーザID
	 */
	public Long id;
	
	/**
	 * 権限
	 */
	public String authority;
	
    /**
     * ユーザー情報
     */
    @ManyToOne
    public User user;
	
	/**
	 * 管理者の場合はTrue、それ以外はFalseを返却する。
	 * @return True/False
	 */
	@JsonIgnore
	public boolean isAdmin(){
		return CommonConstants.ADMIN_AUTHORITY.equals(authority);
	}
	
	/**
	 * セッションから取得したJsonデータをパースして、UserInfoオブジェクトを返却します。<br>
	 * パースエラーの場合はNoneを返却します。
	 * @param data Jsonデータ
	 * @return UserInfo
	 */
	public static Option<UserInfo> getInstance(String data){
		ObjectMapper mapper = new ObjectMapper();
		try {
			return OptionUtil.apply(mapper.readValue(data, UserInfo.class));
		} catch (IOException e) {
			return Option.None();
		}
	}
}
