package controllers;

import interceptors.LoginCheck;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.entity.Book;
import models.entity.Rental;
import models.entity.User;
import models.service.BookService;
import models.service.RentalService;
import models.service.UserService;
import models.session.UserInfo;
import play.Logger;
import play.data.DynamicForm;
import play.libs.F.Option;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import utils.CommonConstants;
import utils.DateUtil;
import utils.MailUtil;

import com.fasterxml.jackson.databind.node.ObjectNode;

@LoginCheck
public class RentalController extends Controller {

	/** 貸出ID */
	private static final String RETURN_JSON_KEY_RENTALID = "id";
	/** 貸出利用者名 */
	private static final String RETURN_JSON_KEY_USERNAME = "userName";
	/** 貸出日 */
	private static final String RETURN_JSON_KEY_RENTALDATE = "rentalDate";

    /**
     * 貸出登録
     * @return
     */
    public static Result lend() {
		// レスポンスJSON
		ObjectNode result = Json.newObject();
		// 入力データ取得
		DynamicForm form =  new DynamicForm().bindFromRequest();

		// ログインユーザ情報の取得を行う
		String userinfoJson =session("userInfo");
		UserService userService = new UserService();
		Option<User> userInfo = userService.findById(UserInfo.getInstance(userinfoJson).get().id);

		// ユーザ検索結果の判定
		if(!userInfo.isDefined()){
			// ログインユーザ情報の取得に失敗した場合
			Logger.error("ログインユーザ情報取得失敗");
			result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
			result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "ログイン情報の取得に失敗しました。<BR>ログインし直してください。");
			return ok(result);
		}
		Logger.info("ログインユーザ情報取得");

		// 書籍の情報を取得する
		BookService bookService = new BookService();
		Option<Book> bookInfo = bookService.findById(Long.parseLong(form.get("id")));

		// 書籍検索結果の判定
		if(!bookInfo.isDefined()){
			// 書籍情報の取得に失敗した場合
			Logger.error("書籍情報取得失敗");
			result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
			result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "貸出に失敗しました。");
			return ok(result);
		}
		Logger.info("書籍情報取得");
		
		User user = userInfo.get();
		Book book = bookInfo.get();

		// 貸出登録
		Rental rental =new Rental();
		rental.user = user;
		rental.book = book;
		rental.rentalDate = DateUtil.getSystemDate();
		rental.returnPlanDate = DateUtil.paseStringToDate(form.get("returnPlanDate"));
		rental.group = userInfo.get().group;

		// 日付の整合性チェック
		if(rental.returnPlanDate == null){
			Logger.error("返却予定日付入力エラー");
			result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
			result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "日付を正しく入力してください。");
			return ok(result);
		}
		if(!DateUtil.compareToDate(rental.rentalDate, rental.returnPlanDate)){
			Logger.error("返却予定日付入力エラー");
			result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
			result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "返却予定日は現在日以降の日付を設定してください。");
			return ok(result);
		}

		RentalService rentalService = new RentalService();
		Option<Rental> rentalInfo = rentalService.save(rental);

		if(rentalInfo.isDefined()){
			// 貸出に成功した場合
			result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_COMPLETE);
			result.put(RETURN_JSON_KEY_RENTALID, rentalInfo.get().id);
			result.put(RETURN_JSON_KEY_USERNAME, userInfo.get().name);
			result.put(RETURN_JSON_KEY_RENTALDATE, DateUtil.paseDateToString(rental.rentalDate));
			
			// メール通知
			Map<String,String> data = new HashMap<>();
	    	data.put("userName",user.name);
	    	data.put("bookName", book.name);
	    	data.put("returnPlanDate",DateUtil.paseDateToString(rental.returnPlanDate));
	    	data.put("rentalDate", DateUtil.paseDateToString(rental.rentalDate));
			MailUtil.send(MailUtil.SUBJECT_RENTAL + user.name, user.group.id, views.html.mail.rental.render(data).toString());
			
			return ok(result);
		} else {
			// 貸出に失敗した場合
			Logger.error("書籍貸出処理失敗");
			result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
			result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "貸出に失敗しました。");
			return ok(result);
		}
	}

    /**
     * 延長登録
     * @param id
     * @return
     */
    public static Result extend(Integer id){
    	return null;
    }

	/**
	 * 返却登録
	 * @param id
	 * @return
	 */
	public static Result ret(Long id){
		ObjectNode result = Json.newObject();
		// 貸出情報を取得する
		DynamicForm form =  new DynamicForm().bindFromRequest();
		RentalService rentalService = new RentalService();
		Option<Rental> rentalInfo = rentalService.findById(id);
		//検索結果の判定
		if(rentalInfo.isDefined()){
			Logger.info("貸出情報取得成功");
			Rental rental =rentalInfo.get();
			rental.returnDate = new Date();
			Option<Rental> optionRentalResult = rentalService.update(rental);
			if(optionRentalResult.isDefined()){
				// 返却に成功した場合
				result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_COMPLETE);
				result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "");
				result.put(RETURN_JSON_KEY_RENTALID, rentalInfo.get().id);
				
				// メール通知
				Map<String,String> data = new HashMap<>();
		    	data.put("userName",rental.user.name);
		    	data.put("bookName", rental.book.name);
		    	data.put("returnDate", DateUtil.paseDateToString(rental.returnDate));
				MailUtil.send(MailUtil.SUBJECT_RETURN + rental.user.name, rental.user.group.id, views.html.mail.returned.render(data).toString());
	    	}else{
				// 返却に失敗した場合
				Logger.info("返却処理失敗");
				result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
				result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "返却に失敗しました。");
	    	}
		}else{
			// 返却に失敗した場合
			Logger.info("貸出情報取得失敗");
			result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
			result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "貸出情報が見つかりません。");
		}
		return ok(result);
    }
    /**
     * 貸出履歴取得
     * 選択した本に対しての貸し出し履歴を返却する。
     *
     * @param id 選択した本のID
     * @return 貸し出し履歴情報(利用者、貸出日、返却日)
     */
    public static Result history(Integer id){
    	List<ObjectNode> resultList = new ArrayList<ObjectNode>();
    	//対象の本の貸出履歴情報の取得
		RentalService rentalService = new RentalService();
		Option<List<Rental>> rentalList = rentalService.findByBookId(id.longValue());

    	for(Rental rental : rentalList.get()){
        	//貸し出し利用者名の取得
    		ObjectNode result = Json.newObject();

    		String rentalDate = "";
    		if(rental.rentalDate != null){
 //   			rentalDate = rental.rentalDate.toString();
    			rentalDate = DateUtil.paseDateToString(rental.rentalDate);
    		}

    		String returnDate = "";
    		if(rental.returnDate != null){
//    			returnDate = rental.returnDate.toString();
    			returnDate = DateUtil.paseDateToString(rental.returnDate);
    		}

    		result.put("rentalUserName",rental.user.name);
    		result.put("rentalDate",rentalDate);
    		result.put("returnDate",returnDate);
    		resultList.add(result);

    	}

    	return ok(Json.toJson(resultList));
    }

    /**
     * 貸出のメールを送信します。
     * @param rental 貸出情報
     */
    private static void sendRental(Rental rental){
    	UserInfo user = UserInfo.getInstance(session("userInfo")).get();

    	Map<String,String> data = new HashMap<>();
    	data.put("bookName",rental.book.name);
    	data.put("returnPlanDate", DateUtil.paseDateToString(rental.returnPlanDate));
    	data.put("rentalDate", DateUtil.paseDateToString(rental.rentalDate));
    	data.put("userName", user.name);

    	MailUtil.send(MailUtil.SUBJECT_RENTAL + user.name, user.groupId, views.html.mail.rental.render(data).toString());
    }

    /**
     * 返却のメールを送信します。
     * @param rental 貸出情報
     */
    private static void sendReturn(Rental rental){
    	UserInfo user = UserInfo.getInstance(session("userInfo")).get();

    	Map<String,String> data = new HashMap<>();
    	data.put("bookName",rental.book.name);
    	data.put("userName", user.name);
    	data.put("returnDate", DateUtil.paseDateToString(rental.returnDate));

    	MailUtil.send(MailUtil.SUBJECT_RETURN + user.name, user.groupId, views.html.mail.returned.render(data).toString());
    }
}
