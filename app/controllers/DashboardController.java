package controllers;

import static play.data.Form.form;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.entity.Book;
import models.entity.Group;
import models.entity.Rental;
import models.entity.Shelf;
import models.entity.User;
import models.form.PasswordForm;
import models.form.UserForm;
import models.response.BookItem;
import models.response.ResultError;
import models.response.ShelfItem;
import models.service.BookService;
import models.service.GroupService;
import models.service.RentalService;
import models.service.ShelfService;
import models.service.UserService;
import models.session.UserInfo;

import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
//import com.sun.org.apache.xpath.internal.compiler.OpMap;

import play.api.data.validation.ValidationError;
import play.data.Form;
import play.libs.Json;
import play.libs.F.Option;
import play.mvc.Controller;
import play.mvc.Result;
import utils.DateUtil;
import views.html.dashboard;

public class DashboardController extends Controller{
	
	private final static String ANOUNCE_KEY ="announce";
	private final static String NOTIFICATION_KEY ="notification";
	private final static String NEWARRIVAL_KEY = "newarrival";
	
	public static Result index(){
    	Map<String,String> optMap = new HashMap<String,String>();
    	GroupService groupService = new GroupService();
		
		//String currentDateStr = DateUtil.paseDateToString(new Date());
		
		// ユーザ情報取得
    	String json = session("userInfo");
    	Option<UserInfo> optUser = UserInfo.getInstance(json);
    	UserInfo userInfo = optUser.get();
		
    	// グループ情報取得
    	Option<Group> optGroup = groupService.findById(userInfo.groupId);
    	if(optGroup.isEmpty()){
    		return notFound();
    	}
    	optMap.put(ANOUNCE_KEY, optGroup.get().anounce);
    	
    	Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.DATE, -1 * optGroup.get().newArrivalSpan);
    	Option<List<Book>> optBookList = BookService.use().findNewArrival(userInfo.groupId, cal.getTime());
    	
    	if(optBookList.isDefined() && optBookList.get().size() > 0){
    		StringBuilder sb = new StringBuilder();
    		for(Book book : optBookList.get()){
    			sb.append(DateUtil.paseDateToString(book.createDate));
    			sb.append(" ");
    			sb.append(book.name);
    			sb.append(System.getProperty("line.separator"));
    		}
    		optMap.put(NEWARRIVAL_KEY, sb.toString());
    	}else{
    		optMap.put(NEWARRIVAL_KEY, "");
    	}
    	
    	Option<List<Rental>> optRentalList = RentalService.use().findExcessByGroupId(userInfo.groupId);
    	
    	if(optRentalList.isDefined() && optRentalList.get().size() > 0){
    		
    		String message = "返却予定日が過ぎている本が"+optRentalList.get().size()+"件あります。";
        	optMap.put(NOTIFICATION_KEY, message);
    	}else{
    		optMap.put(NOTIFICATION_KEY, "");
		}
    	
    	List<SqlRow> rankList = RentalService.use().findRanking(userInfo.groupId);
    	
    	return ok(dashboard.render(optMap,rankList));
	}
}
