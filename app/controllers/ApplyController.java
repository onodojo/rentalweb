package controllers;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import interceptors.AdminCheck;
import interceptors.LoginCheck;
import models.entity.Apply;
import models.entity.Group;
import models.entity.User;
import models.form.ApplyForm;
import models.response.ApplyItem;
import models.service.ApplyService;
import models.service.GroupService;
import models.service.UserService;
import models.session.UserInfo;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.F.Option;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import utils.CommonConstants;
import utils.DateUtil;
import utils.MailUtil;
import views.html.apply;

@LoginCheck
public class ApplyController extends Controller {

    // ブックタイプが「本」だった場合
    private static String BOOK_TYPE_BOOK_VALUE = "1";

    // ブックタイプが「電子書籍」だった場合
    private static String BOOK_TYPE_E_BOOK_VALUE = "2";

    // ブックタイプが「本」だった場合
    private static String BOOK_TYPE_BOOK_NAME = "本";

    // ブックタイプが「電子書籍」だった場合
    private static String BOOK_TYPE_E_BOOK_NAME = "電子";
    
    private static final String BUYER_COMPANY = "会社";
    
    private static final String BUYER_PERSON = "個人";
    
    private static final String BUYER_COMPANY_VALUE = "1";
    private static final String BUYER_PERSON_VALUE = "2";

    // Jsonで返却するKEY項目
    private static String JSON_RETURN_KEY_REQUESTNO = "requestNo";
    private static String JSON_RETURN_KEY_APPLY_ID = "id";
    private static String JSON_RETURN_KEY_ISBN = "isbn";
    private static String JSON_RETURN_KEY_BOOKNAME = "bookName";
    private static String JSON_RETURN_KEY_APPLY_DATE = "applyDate";
    private static String JSON_RETURN_KEY_USERNAME = "user";
    private static String JSON_RETURN_KEY_BUYER = "buyer";
    private static String JSON_RETURN_KEY_BOOKTYPE = "bookType";
    private static String JSON_RETURN_KEY_PRICE = "price";
    private static String JSON_RETURN_KEY_VOLUME = "volume";
    private static String JSON_RETURN_KEY_REASON = "reason";

    // 現在時刻
    private static Date currentTime = new Date();

    /**
     * 申請一覧
     *
     * @return List<ApplyItem>
     */
    public static Result index() {
        return ok(apply.render(""));
    }

    public static Result findList(){
        String filter = request().getQueryString("f");

        // セッションからグループIDを取得
        Option<UserInfo> u = UserInfo.getInstance(session("userInfo"));

        Option<List<Apply>> applyList;
        if(u.get().isAdmin()) {
            applyList = ApplyService.use().findByFilter(u.get().groupId, filter);
        }else{
            applyList = ApplyService.use().findByUserId(u.get().id,filter);
        }

        ObjectNode result = Json.newObject();
        List<ObjectNode> nodeList = new ArrayList<>();

        if(!applyList.isEmpty() && !applyList.get().isEmpty()){
            // 該当データなし
            for(Apply apply : applyList.get()){
                ObjectNode node = Json.newObject();
                node.put("id",apply.id);
                node.put("requestNo",apply.requestNo);
                node.put("user",apply.user.name);
                node.put("applyDate",DateUtil.paseDateToString(apply.applyDate));
                node.put("isbn",apply.isbn);
                node.put("bookName",apply.bookName);
                nodeList.add(node);
            }
        }

        result.putPOJO("data",nodeList);

        return ok(Json.toJson(result));
    }

    /**
     * 承認済み書籍一覧の取得
     *
     * @return
     */
    public static Result findbyGroupId4Approved() {

        // 承認済み書籍の一覧を取得します。
        UserInfo userInfo = UserInfo.getInstance(session("userInfo")).get();
        List<Apply> applyList = Apply.find.where().eq("group_id", userInfo.groupId).eq("apply_state", CommonConstants.APPLY_STATE_AUTHORIZED).findList();

        List<ObjectNode> nodeList = new ArrayList<ObjectNode>();
        for (Apply apply : applyList) {
            ObjectNode applyinfo = Json.newObject();
            applyinfo.put("id", apply.id);
            applyinfo.put("user", apply.user.name);
            applyinfo.put("applyDate", DateUtil.paseDateToString(apply.applyDate));
            applyinfo.put("authorizeDate", DateUtil.paseDateToString(apply.authorizeDate));
            applyinfo.put("isbn", apply.isbn);
            applyinfo.put("booktype", apply.bookType);
            nodeList.add(applyinfo);
        }
        return ok(Json.toJson(nodeList));
    }

    /**
     * 書籍申請
     *
     * @return json
     */
    public static Result apply() {

        Form<ApplyForm> form = Form.form(ApplyForm.class).bindFromRequest();

        if(form.hasErrors()){
            return badRequest(form.errorsAsJson());
        }

        ApplyForm applyForm = form.get();

        Apply apply = new Apply();
        // セッション情報の取得
        String userinfoJson = session("userInfo");

        // セッションからグループIDを取得
        Option<UserInfo> u = UserInfo.getInstance(userinfoJson);
        long userId = u.get().id;
        long groupId = u.get().groupId;

        // 申請の登録処理
        apply.isbn = applyForm.isbn;
        apply.applyDate = new Date();
        apply.bookName = applyForm.bookName;
        apply.bookType = applyForm.bookType;
        apply.buyer = applyForm.buyer;
        apply.price = Integer.parseInt(applyForm.price);
        apply.volume = Integer.parseInt(applyForm.volume);
        apply.reason = applyForm.reason;
        apply.requestNo = String.valueOf(new Date().getTime());
        apply.url = applyForm.url;

        // ユーザ情報の設定
        UserService userService = new UserService();
        Option<User> userInfo = userService.findById(userId);
        apply.user = userInfo.get();

        // グループ情報の設定
        GroupService groupService = new GroupService();
        Option<Group> groupInfo = groupService.findById(groupId);
        apply.group = groupInfo.get();
        apply.applyState = CommonConstants.APPLY_STATE_PENDING;

        ApplyService applyService = new ApplyService();
        Option<Apply> rentalInfo = applyService.save(apply);

        // 返却
        ObjectNode result = Json.newObject();
        if (rentalInfo.isDefined()) {
            // 申請に成功した場合
            result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_COMPLETE);

            // Apply情報をセット
            result.put(JSON_RETURN_KEY_APPLY_ID, rentalInfo.get().id);
            result.put(JSON_RETURN_KEY_ISBN, rentalInfo.get().isbn);
            result.put(JSON_RETURN_KEY_REQUESTNO, rentalInfo.get().requestNo);
            result.put(JSON_RETURN_KEY_BOOKNAME, rentalInfo.get().bookName);
            result.put(JSON_RETURN_KEY_BUYER, rentalInfo.get().buyer);
            result.put(JSON_RETURN_KEY_APPLY_DATE, DateUtil.paseDateToString(rentalInfo.get().applyDate));
            result.put(JSON_RETURN_KEY_USERNAME, rentalInfo.get().user.name);
            result.put(JSON_RETURN_KEY_BOOKTYPE, rentalInfo.get().bookType);
            result.put(JSON_RETURN_KEY_PRICE, String.valueOf(rentalInfo.get().price));
            result.put(JSON_RETURN_KEY_VOLUME, String.valueOf(rentalInfo.get().volume));
            result.put(JSON_RETURN_KEY_REASON, rentalInfo.get().reason);

            sendRequest(apply);
            return ok(result);

        } else {
            // 申請に失敗した場合
            result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
            result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "書籍の申請に失敗しました。");
            return ok(result);
        }
    }

    public static Result getRequest(Long requestId){
        final Option<Apply> apply = ApplyService.use().findById(requestId);
        if(apply.isEmpty()){
            return notFound();
        }

        // セッションからグループIDを取得
        Option<UserInfo> u = UserInfo.getInstance(session("userInfo"));

        Apply applyEntity = apply.get();
        ObjectNode result = Json.newObject();
        ObjectNode request = Json.newObject();
        request.put("isbn",applyEntity.isbn);
        request.put("bookName",applyEntity.bookName);
        // 書籍の種類のデータ整形
        if (BOOK_TYPE_BOOK_VALUE.equals(applyEntity.bookType)) {
            // 書籍の種類が「本」の場合
            request.put("bookType",BOOK_TYPE_BOOK_NAME);
        } else if (BOOK_TYPE_E_BOOK_VALUE.equals(applyEntity.bookType)) {
            // 書籍の種類が「電子」の場合
            request.put("bookType",BOOK_TYPE_E_BOOK_NAME);
        }
        
        if (BUYER_COMPANY_VALUE.equals(applyEntity.buyer)) {
            request.put("buyer",BUYER_COMPANY);
        } else if (BUYER_PERSON_VALUE.equals(applyEntity.buyer)) {
            request.put("buyer",BUYER_PERSON);
        }

        request.put("reason",applyEntity.reason);
        request.put("volume",applyEntity.volume);
        NumberFormat nfNum = NumberFormat.getCurrencyInstance(Locale.JAPAN);
        request.put("price",nfNum.format(applyEntity.price));
        request.put("url",applyEntity.url);
        request.put("isAdmin",u.get().isAdmin());

        result.put(CommonConstants.RETURN_JSON_KEY_RESULT,CommonConstants.RETURN_JSON_VALUE_RESULT_COMPLETE);
        result.put("data",request);

        return ok(result);
    }

    /**
     * approval()
     *
     * @param id
     * @return
     */
    @AdminCheck
    public static Result approval(Integer id) {
        ApplyService applyService = new ApplyService();
        Option<Apply> applyResult = applyService.findById(id.longValue());
        Apply apply = applyResult.get();
        ObjectNode result = Json.newObject();
        Option<UserInfo> userInfo = UserInfo.getInstance(session("userInfo"));
        if (applyResult.isDefined()) {
            apply.authorizer = new User(userInfo.get().id);
            apply.authorizeDate = currentTime;
            apply.applyState = CommonConstants.APPLY_STATE_AUTHORIZED;
            applyService.save(apply);
            result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_COMPLETE);

            sendApprove(apply);
        } else {
            result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
            result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "書籍の申請情報が取得できませんでした");
        }
        return ok(result);
    }

    /**
     * cancel()
     *
     * @param id
     * @return
     */
    public static Result cancel(Integer id) {

        ApplyService applyService = new ApplyService();
        Option<Apply> applyResult = applyService.findById(id.longValue());
        Apply apply = applyResult.get();
        ObjectNode result = Json.newObject();
        Option<UserInfo> userInfo = UserInfo.getInstance(session("userInfo"));
        UserInfo ui = userInfo.get();

        if (!applyResult.isDefined()) {
            result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
            result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "書籍の申請情報が取得できませんでした");
            return ok(result);
        }

        if (CommonConstants.USER_AUTHORITY.equals(ui.authority) && CommonConstants.APPLY_STATE_PENDING.equals(apply.applyState)) {
            result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
            result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "対象書籍が承認されてないため、削除できません");
            return ok(result);

        }

        User user = userInfo.get().user;
        apply.authorizer = new User(ui.id);
        apply.authorizeDate = currentTime;
        apply.applyState = CommonConstants.APPLY_STATE_WITHDRAWAL;
        applyService.save(apply);
        result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_COMPLETE);

        sendCancel(apply);
        return ok(result);
    }

    /**
     * delete()
     *
     * @param id
     * @return
     */
    public static Result delete(Integer id) {

        ApplyService applyService = new ApplyService();
        Option<Apply> apply = applyService.findById(id.longValue());
        Option<UserInfo> userInfo = UserInfo.getInstance(session("userInfo"));
        ObjectNode result = Json.newObject();

        if (apply.isEmpty()) {
            result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
            result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "対象書籍が見つかりませんでした");
            return ok(result);
        }

        if (CommonConstants.USER_AUTHORITY.equals(userInfo.get().authority) && CommonConstants.APPLY_STATE_AUTHORIZED.equals(apply.get().applyState)) {
            result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
            result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "承認済み書籍であるため、削除できません");
            return ok(result);
        }

        applyService.delete(apply.get());
        result.put(CommonConstants.RETURN_JSON_KEY_RESULT, CommonConstants.RETURN_JSON_VALUE_RESULT_COMPLETE);
        return ok(result);
    }

    /**
     * 申請のメールを送信します。
     *
     * @param apply 申請情報
     */
    private static void sendRequest(Apply apply) {
        UserInfo user = UserInfo.getInstance(session("userInfo")).get();

        Map<String, String> data = new HashMap<>();
        data.put("requestNo", apply.requestNo);
        data.put("userName", user.name);

        MailUtil.send(MailUtil.SUBJECT_REQUEST + user.name, user.groupId, views.html.mail.request.render(data).toString());
    }

    /**
     * 取下のメールを送信します。
     *
     * @param apply 申請情報
     */
    private static void sendCancel(Apply apply) {
        UserInfo user = UserInfo.getInstance(session("userInfo")).get();

        Map<String, String> data = new HashMap<>();
        data.put("requestNo", apply.requestNo);
        data.put("userName", user.name);

        MailUtil.send(MailUtil.SUBJECT_CANCEL + user.name, user.groupId, views.html.mail.cancel.render(data).toString());
    }

    /**
     * 承認のメールを送信します。
     *
     * @param apply 申請情報
     */
    private static void sendApprove(Apply apply) {
        UserInfo user = UserInfo.getInstance(session("userInfo")).get();

        Map<String, String> data = new HashMap<>();
        data.put("requestNo", apply.requestNo);
        data.put("userName", user.name);
        data.put("bookName", apply.bookName);

        MailUtil.send2User(MailUtil.SUBJECT_APPROVE, user.id, views.html.mail.approve.render(data).toString());
    }

}