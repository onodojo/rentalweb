package controllers;

import static play.data.Form.form;

import java.util.ArrayList;

import org.apache.commons.lang3.RandomStringUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import models.entity.Group;
import models.entity.User;
import models.form.SignupForm;
import models.form.UserForm;
import models.service.GroupService;
import models.session.UserInfo;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.libs.F.Option;
import utils.CommonConstants;
import views.html.*;

public class SignupController extends Controller {

    public static Result index() {
    	return ok(views.html.signup.render("サインアップ",form(SignupForm.class)));
    }

    /**
     * ユーザ、グループ情報を登録し、書籍一覧画面に遷移します。
     * @return Result
     * @throws Exception
     */
    public static Result signup() throws Exception{    	
    	
    	// リクエスト情報を取得
    	Form<SignupForm> f = form(SignupForm.class).bindFromRequest();

    	if(f.hasErrors()){
    		return badRequest(views.html.signup.render("サインアップ",f));
    	}
    	SignupForm form = f.get();
    	if(form.invalidPassword()){
    		// パスワードが異なる場合
    		flash("invalidPassword","パスワードが一致しません。");
    		return ok(views.html.signup.render("サインアップ",f));
    	}
    	
    	User user = new User();
    	user.mailaddress = form.mailaddress;
    	user.name = form.name;
    	user.password = form.password;
    	user.authority = CommonConstants.ADMIN_AUTHORITY;
    	
    	Group group = new Group();
    	group.name = form.groupName;
    	group.userList = new ArrayList<User>();
    	group.userList.add(user);
    	group.token = RandomStringUtils.randomAlphanumeric(20);
    	
    	// 登録
    	Option<Group> optGroup = GroupService.use().save(group);
    	
    	if(optGroup.isEmpty()){
    		// エラー
    		return notFound();
    	}
    	
    	session("token",group.token);
    	
    	saveSession(user);
    	
    	return redirect(controllers.routes.BookController.index());
    }
    
	/**
	 * セッションにログインユーザ情報を格納します。
	 * @param user ログインユーザ情報
	 */
	protected static void saveSession(final User user) throws Exception{
		
		// ユーザ情報をセッションに格納
		UserInfo userInfo = new UserInfo();
		userInfo.id = user.id;
		userInfo.groupId = user.group.id;
		userInfo.name = user.name;
		userInfo.authority = user.authority;
		
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(userInfo);
		
		session("userInfo",json);
		session("userName",userInfo.name);
		session("authority",userInfo.authority);
		session("collectflg",String.valueOf(userInfo.groupId == CommonConstants.COLLECT_GROUP_ID));
	}

}
