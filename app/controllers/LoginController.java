package controllers;


import static play.data.Form.form;

import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

import com.fasterxml.jackson.databind.ObjectMapper;

import models.entity.Group;
import models.entity.User;
import models.form.BookForm;
import models.form.LoginForm;
import models.service.GroupService;
import models.service.UserService;
import models.session.UserInfo;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.WS;
import play.libs.F.Option;
import play.libs.F.Promise;
import play.libs.WS.Response;
import play.libs.WS.WSRequestHolder;
import play.mvc.Controller;
import play.mvc.Http.Cookie;
import play.mvc.Http.Cookies;
import play.mvc.Result;
import play.mvc.SimpleResult;
import utils.StringUtil;
import views.html.login;

public class LoginController extends Controller {

	/**
	 * ログイン画面を表示します。
	 * @return ログイン画面
	 */
	public static Result index() {
		LoginForm form = new LoginForm();
	
		Map<String, String[]> queryStrings = request().queryString();
		if(queryStrings.containsKey("token")){
			
			String token = queryStrings.get("token")[0];
			if(!"".equals(token)){
				Option<Group> optGroup = GroupService.use().findByToken(token);
				if(optGroup.isEmpty()){
					return badRequest("Not Found");
				}
				
				session("token",token);
				form.groupId = String.valueOf(optGroup.get().id);
			}
		}
		return ok(login.render(form));
	}

	/**
	 * ログイン処理を行います。
	 * @return 
	 */
	public static Result login() throws Exception{
		
		Form<LoginForm> f = form(LoginForm.class).bindFromRequest();
		
		if(StringUtil.isBlank(f.get().groupId)){
			flash("error.login","入力されたアカウントは存在しません。");
			return badRequest(login.render(f.get()));
		}
		
		Option<User> oUser = UserService.use().find(f.get().mailaddress, f.get().password,Long.parseLong(f.get().groupId));
		
		if(oUser.isEmpty()){
			flash("error.login","入力されたアカウントは存在しません。");
			return badRequest(login.render(f.get()));
		}
		
		// ユーザ情報をセッションに格納
		User user = oUser.get();
		SignupController.saveSession(user);
//    	UserInfo userInfo = new UserInfo();
//    	userInfo.id = user.id;
//    	userInfo.groupId = user.group.id;
//    	userInfo.name = user.name;
//    	userInfo.authority = user.authority;
//    	
//    	ObjectMapper mapper = new ObjectMapper();
//    	String json = mapper.writeValueAsString(userInfo);
//    	
//    	session("userInfo",json);
//    	session("userName",userInfo.name);
//    	session("authority",userInfo.authority);
		
		// 書籍一覧にリダイレクト
		return redirect(controllers.routes.BookController.index());
	}

	/**
	 * ログアウト処理
	 * @return ログイン画面
	 * @throws Exception
	 */
	public static Result logout() throws Exception{
		
		// セッション情報削除
		session().remove("userInfo");
		session().remove("userName");
		session().remove("authority");
		session().remove("collectflg");

		return redirect("login?token=" + session("token"));
	}

	/**
	 * GroupSession連携用
	 * @return
	 * @throws Exception
	 */
	public static Result auto() throws Exception{
		
		// トークンの存在チェック
		Map<String, String[]> queryStrings = request().queryString();
		if(!queryStrings.containsKey("token")){
			return badRequest("無効なトークンです。");
		}
		
		// グループの存在チェック
		String token = queryStrings.get("token")[0];
		Option<Group> optGroup = GroupService.use().findByToken(token);
		if(optGroup.isEmpty()){
			return badRequest("グループが存在しません。");
		}
		
		// CookieからJSESSIONIDを取得
		String jsessionId = request().cookie("JSESSIONID").value();
	
		if(StringUtil.isBlank(jsessionId)){
			// JSESSIONIDが空の場合はログイン画面へリダイレクト
			return redirect("/syoseki/login?token=" + token);
		}
		
		// GroupSessionのログイン認証チェック
		Response response = WS.url("http://localhost:8080/gsession/api/custom/session/auth.do")
				.setHeader("Cookie", "JSESSIONID=" + jsessionId)
				.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.63 Safari/537.36")
				.setQueryParameter("JSESSIONID", jsessionId).get().get(1000);
		
		if(response.getStatus() == 200){
			XPath xpath = XPathFactory.newInstance().newXPath();
			Document document = response.asXml();
			String status = xpath.evaluate("/ResultSet/Result/Status/text()", document);
			
			if("0".equals(status)){
				
				// GroupSessionのログインIDを取得
				String loginId = xpath.evaluate("/ResultSet/Result/LoginId/text()", document);
				
				Option<User> optUser = UserService.use().findByMailAddress(loginId + "@dcom-web.co.jp", optGroup.get().id);
				if(optUser.isEmpty()){
					// 該当のユーザが存在しない場合はログイン画面にリダイレクト
//					return redirect("/login?token=" + token);
					return redirect("/syoseki/login?token=" + token);
				}
				
				User user = optUser.get();
		    	SignupController.saveSession(user);
//		    	UserInfo userInfo = new UserInfo();
//		    	userInfo.id = user.id;
//		    	userInfo.groupId = user.group.id;
//		    	userInfo.name = user.name;
//		    	userInfo.authority = user.authority;
//		    	
//		    	ObjectMapper mapper = new ObjectMapper();
//		    	String json = mapper.writeValueAsString(userInfo);
//		    	
//		    	session("userInfo",json);
//		    	session("userName",userInfo.name);
//		    	session("authority",userInfo.authority);
				session("token",token);
				
				// 書籍一覧にリダイレクト
				return redirect(controllers.routes.BookController.index());
			}
		}
		return redirect("/syoseki/login?token=" + token);
	}
}
