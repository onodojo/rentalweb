package controllers;

import static play.data.Form.*;
import interceptors.AdminCheck;
import interceptors.LoginCheck;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.entity.Group;
import models.entity.User;
import models.form.UserForm;
import models.response.ResultError;
import models.service.UserService;
import models.session.UserInfo;
import play.Logger;
import play.data.Form;
import play.libs.F.Option;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Http.Session;
import play.mvc.Result;
import utils.CommonConstants;
import views.html.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@LoginCheck
@AdminCheck
public class UserController extends Controller {

	/**
	 * ユーザ管理画面の表示
	 *
	 * @return
	 */
	public static Result index() {
		return ok(user.render("",form(UserForm.class)));
	}

	/**
	 * ユーザリストを取得する。
	 * @return
	 */
	public static Result list() {
		// セッション情報の取得
		String userinfoJson =session("userInfo");
		Option<UserInfo> u = UserInfo.getInstance(userinfoJson);
		long groupId = 1;
		groupId = u.get().groupId;

		UserService userService = new UserService();
		// グループに紐付くユーザリストを取得
		Option<List<User>> userList = userService.findByGroupId(groupId);
		List<ObjectNode> nodeList = new ArrayList<ObjectNode>();

		for (User result : userList.get()) {
			// 権限
			String authority = "";
			if(result.authority.equals(CommonConstants.ADMIN_AUTHORITY)){
				authority = "管理者";
			}else if(result.authority.equals(CommonConstants.USER_AUTHORITY)){
				authority = "利用者";
			}

			ObjectNode user = Json.newObject();
			user.put("id", result.id);
			user.put("name", result.name);
			user.put("mailaddress", result.mailaddress);
			user.put("authority", authority);
			nodeList.add(user);
		}

		return ok(Json.toJson(nodeList));
	}

	/**
	 * idに紐づいたユーザ情報を返却します。
	 * 
	 * @param id ユーザID
	 * @return ユーザ情報
	 */
	public static Result find(Long id) throws Exception{
		Option<User> optUser = UserService.use().findById(id);
		if(optUser.isEmpty()){
			return badRequest("ユーザが存在しません。");
		}
		User user = optUser.get();
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> map = new HashMap<>();
		map.put("id", user.id);
		map.put("name", user.name);
		map.put("password", user.password);
		map.put("mailaddress", user.mailaddress);
		map.put("authority", user.authority);
		
		return ok(mapper.writeValueAsString(map));
	}

	/**
	 * ユーザの新規登録を行う
	 *
	 * @return
	 * @throws JsonProcessingException
	 */
	@LoginCheck
	public static Result create() throws JsonProcessingException {
		//入力情報を取得
		Form<UserForm> f = form(UserForm.class).bindFromRequest();
		if(f.hasErrors()){
			ObjectMapper mapper = new ObjectMapper();
			return ok(mapper.writeValueAsString(new ResultError(f.errors())));
		}

		// セッション情報の取得
		String userinfoJson =session("userInfo");
		// セッションからグループIDを取得
		Option<UserInfo> u = UserInfo.getInstance(userinfoJson);
		long groupId = 1;
//    	if(u.isDefined()){
			// グループIDの取得
			groupId = u.get().groupId;
//    	} else {
//	   		 // セッション情報が存在しない場合
//	   		System.out.println("セッション情報がありません。");
//	   	//	return badRequest("","");
//    	}

		UserForm form = f.get();
		String userName = form.name;
		String mail = form.mailaddress;
		String authority = form.authority;
		String password = form.password;

		//ユーザ情報を登録
		User userInfo = new User();
		userInfo.name = form.name;
		userInfo.mailaddress = form.mailaddress;
		userInfo.authority = form.authority;
		userInfo.password = form.password;
		//グループID(固定で1)
		userInfo.group = new Group();
		userInfo.group.id = groupId;
		
		UserService userService = new UserService();
		Option<User> userResult = userService.save(userInfo);
		if(userResult.isDefined()){
			// 権限
			if(userResult.get().authority.equals(CommonConstants.ADMIN_AUTHORITY)){
				userInfo.authority = "管理者";
			}else if(userResult.get().authority.equals(CommonConstants.USER_AUTHORITY)){
				userInfo.authority = "利用者";
			}
			ObjectNode result = Json.newObject();
			result.put("id", userResult.get().id);
			result.put("name", userResult.get().name);
			result.put("mailaddress", userResult.get().mailaddress);
			result.put("authority", userInfo.authority);
			return ok(result);
		}else{
			Logger.debug("save error");
			return ok("");
		}
	}

	/**
	 * ユーザ情報の更新を行う
	 *
	 * @param id
	 * @return
	 * @throws JsonProcessingException
	 */
	public static Result update(Integer id) throws JsonProcessingException {
		//Logger.debug(id.toString());
		//入力情報を取得
		Form<UserForm> f = form(UserForm.class).bindFromRequest();
		if(f.hasErrors()){
			ObjectMapper mapper = new ObjectMapper();
			return ok(mapper.writeValueAsString(new ResultError(f.errors())));
		}
		// セッション情報の取得
		String userinfoJson =session("userInfo");
		Option<UserInfo> u = UserInfo.getInstance(userinfoJson);
		long groupId ;
		groupId = u.get().groupId;
		
		UserForm form = f.get();
		String name = form.name;
		String mailaddress = form.mailaddress;
		String authority = form.authority;
		String password = form.password;

		//IDから情報取得
		UserService userService = new UserService();
		Option<User> userResult = userService.findById(id.longValue());

		if(userResult.isDefined()){
			//ユーザ情報の更新
			User user = userResult.get();
			user.name = name;
			user.mailaddress = mailaddress;
			user.authority = authority;
			user.password = password;
			userResult = userService.update(user);
			if(userResult.isDefined()){
				// 権限
				if(userResult.get().authority.equals(CommonConstants.ADMIN_AUTHORITY)){
					authority = "管理者";
				}else if(userResult.get().authority.equals(CommonConstants.USER_AUTHORITY)){
					authority = "利用者";
				}
				ObjectNode result = Json.newObject();
				result.put("id", userResult.get().id);
				result.put("name", userResult.get().name);
				result.put("mailaddress", userResult.get().mailaddress);
				result.put("authority", authority);
				return ok(result);
			}else{
				Logger.debug("update error");
				return ok("");
			}
		}else{
			Logger.debug("update error");
			return ok("");
		}
	}

	/**
	 * ユーザの削除処理を行う
	 *
	 * @param id
	 * @return
	 */
	public static Result delete(Integer id) {
		//ユーザ情報を削除
		UserService userService = new UserService();
		ObjectNode result = Json.newObject();

		Session session = Http.Context.current().session();
		String userinfoJson = session("userInfo");
		Option<UserInfo> u = UserInfo.getInstance(userinfoJson);
		System.out.println(id);
		System.out.println(u.get().id);
		int formId = new Integer (u.get().id.toString());
		if(id==formId){
			result.put("msg", "ログインユーザは削除できません");
			result.put("flg", "0");
			return ok(result);
		}
		Option<User> userResult = userService.deleteById(id.longValue());
		result.put("msg", "削除しました。");
		result.put("flg", "1");
		return ok(result);
	}

	/**
	 * CSVアップロード
	 * 
	 * @return
	 * @throws IOException
	 */
	public static Result upload() throws IOException {
		ObjectNode res = Json.newObject();
		// CSVファイル取得
		File file = request().body().asMultipartFormData().getFiles().get(0).getFile();

		if (!file.isFile()) {
			return null;
		}

		Option<UserInfo> u = UserInfo.getInstance(session("userInfo"));
		UserService.use().updateByCsv(file, u.get().groupId);
		res.put("result", "0");

		return ok(Json.toJson(res));
	}

	/**
	 * CSVダウンロード
	 * 
	 * @return
	 * @throws IOException
	 */
	public static Result download() throws IOException{

		File file = new File("UserInfo.csv");
		UserService userService = new UserService();
		Option<UserInfo> userInfo = UserInfo.getInstance(session("userInfo"));
	
		userService.outputByCsv(file, userInfo.get().groupId);
		response().setHeader("Content-Disposition", "attachment; fileName=UserInfo.csv");
		return ok(file);
	}
	
	/**
	 * ユーザ一括登録(オプション)
	 * 
	 * @return
	 */
	public static Result collectUpload() {
		Option<UserInfo> userInfo = UserInfo.getInstance(session("userInfo"));
		UserService userService = new UserService();
		userService.collectUploadByGeppouDb(userInfo.get().groupId);
		return ok();
	}
}
