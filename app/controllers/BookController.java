package controllers;

import static play.data.Form.form;
import interceptors.AdminCheck;
import interceptors.LoginCheck;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.entity.Apply;
import models.entity.Book;
import models.entity.Group;
import models.entity.Rental;
import models.entity.Shelf;
import models.form.BookForm;
import models.response.BookItem;
import models.response.ShelfItem;
import models.service.ApplyService;
import models.service.BookService;
import models.service.GroupService;
import models.service.ShelfService;
import models.session.UserInfo;
import play.Logger;
import play.data.Form;
import play.libs.F.Option;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import utils.CommonConstants;
import utils.DateUtil;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * 貸出管理処理
 *
 * @author dcom
 */
@LoginCheck
public class BookController extends Controller {

	// 書籍一覧表示時に渡す情報のキー
	private final static String ANOUNCE_KEY = "announce";
	private final static String NOTIFICATION_KEY = "notification";
	private final static String USER_ID_KEY = "userId";

	/** 書籍情報絞り込み条件[全件] */
	private final static Integer BOOK_ALL = 0;
	/** 書籍情報絞り込み条件[貸出可能] */
	private final static Integer BOOK_CAN_RENTAL = 1;
	/** 書籍情報絞り込み条件[貸出中] */
	private final static Integer BOOK_RENTALED = 2;

	/**
	 * 書籍一覧初期表示
	 *
	 * @return
	 */
	public static Result index() {
		List<BookItem> bookItemList = new ArrayList<BookItem>();
		Map<String, Object> optMap = new HashMap<String, Object>();
		List<ShelfItem> shelfItemList = new ArrayList<ShelfItem>();
		GroupService groupService = new GroupService();
		ShelfService shelfService = new ShelfService();

		String currentDateStr = DateUtil.paseDateToString(new Date());
		int expirationCount = 0;

		// ユーザ情報取得
		String json = session("userInfo");
		Option<UserInfo> optUser = UserInfo.getInstance(json);
		UserInfo userInfo = optUser.get();
		optMap.put(USER_ID_KEY, userInfo.id);

		// グループ情報取得
		Option<Group> optGroup = groupService.findById(userInfo.groupId);
		if (optGroup.isEmpty()) {
			return notFound();
		}
		optMap.put(ANOUNCE_KEY, optGroup.get().anounce);

		// 場所/機器情報取得
		Option<List<Shelf>> optShelfList = shelfService
				.findByGroupId(userInfo.groupId);
		if (optShelfList.isDefined()) {
			List<Shelf> shelfList = optShelfList.get();
			for (Shelf shelf : shelfList) {
				ShelfItem shelfItem = new ShelfItem();
				shelfItem.id = shelf.id;
				shelfItem.name = shelf.name;
				shelfItem.kind = shelf.kind;
				shelfItemList.add(shelfItem);
			}
		}

		// 書籍一覧取得
		Option<List<Book>> optBookList = BookService.use().findByGroupId(
				userInfo.groupId);
		if (optBookList.isDefined()) {
			// グループIDに紐付く書籍リストを取得
			List<Book> bookList = optBookList.get();
			for (Book book : bookList) {
				BookItem bookItem = new BookItem();
				// bookListの中身をbookItemListに詰めなおす
				bookItem.id = book.id;
				bookItem.isbn = book.isbn;
				bookItem.url = book.url;
				bookItem.name = book.name;
				bookItem.publisher = book.publisher;
				bookItem.author = book.author;
				if (book.shelf != null) {
					bookItem.shelfId = book.shelf.id;
					bookItem.shelfName = book.shelf.name;
				}
				// bookItem.bookType = book.bookType;
				bookItem.recommend = book.recommend;
				if (book.isRentaled()) {
					Rental rental = book.rentalList.get(0);
					bookItem.rentalUserId = rental.user.id;
					bookItem.rentalDate = DateUtil
							.paseDateToString(rental.rentalDate);
					bookItem.returnPlanDate = DateUtil
							.paseDateToString(rental.returnPlanDate);
					bookItem.userName = rental.user.name;
					bookItem.rentalId = rental.id;
					// 返却予定日が現在日付を過ぎている書籍をカウントする
					if (currentDateStr.compareTo(bookItem.returnPlanDate) > 0) {
						expirationCount++;
					}
				}
				bookItemList.add(bookItem);
			}
			if (expirationCount > 0) {
				String message = "返却予定日が過ぎている本が" + expirationCount + "件あります。";
				optMap.put(NOTIFICATION_KEY, message);
			} else {
				optMap.put(NOTIFICATION_KEY, "");
			}
			return ok(views.html.book.render(bookItemList, optMap,
					shelfItemList));
		}
		return notFound();
	}

	/**
	 * 書籍一覧絞り込み
	 *
	 * @param p
	 * @return
	 */
	public static Result search(Integer condition) {
		Logger.info("書籍一覧絞り込み[start]");

		ObjectNode result = Json.newObject();
		List<BookItem> bookItemList = new ArrayList<BookItem>();

		// ユーザ情報取得
		Option<UserInfo> optUser = UserInfo.getInstance(session("userInfo"));
		if (optUser.isEmpty()) {
			return notFound();
		}

		// グループ情報取得
		Option<Group> optGroup = GroupService.use().findById(
				optUser.get().groupId);
		if (optGroup.isEmpty()) {
			return notFound();
		}

		// 書籍一覧取得
		Option<List<Book>> optBookList = BookService.use().findByGroupId(
				optUser.get().groupId);
		if (BOOK_ALL.equals(condition)) {
			// 全件
			for (Book book : optBookList.get()) {
				bookItemList.add(new BookItem(book));
			}
		} else if (BOOK_CAN_RENTAL.equals(condition)) {
			// 貸出可能書籍
			for (Book book : optBookList.get()) {
				if (!book.isRentaled() && book.shelf != null) {
					bookItemList.add(new BookItem(book));
				}
			}
		} else if (BOOK_RENTALED.equals(condition)) {
			// 貸出中書籍
			for (Book book : optBookList.get()) {
				if (book.isRentaled() && book.shelf != null) {
					bookItemList.add(new BookItem(book));
				}
			}
		} else {
			return notFound();
		}

		result.put("bookList", Json.toJson(bookItemList));
		result.put("userId", optUser.get().id);
		Logger.info("書籍一覧絞り込み[end]");
		return ok(result);
	}

	public static Result find(Integer id) {
		return null;
	}

	/**
	 * create():書籍登録
	 *
	 * @return
	 */
	@AdminCheck
	public static Result create() {
		ObjectNode result = Json.newObject();
		List<BookItem> bookItemList = new ArrayList<BookItem>();
		BookService bookservice = new BookService();
		ShelfService shelfService = new ShelfService();
		
		// ユーザ情報取得
		Option<UserInfo> optUser = UserInfo.getInstance(session("userInfo"));
		if (optUser.isEmpty()) {
			return notFound();
		}
		
		// 入力情報を取得
		Form<BookForm> f = form(BookForm.class).bindFromRequest();
		BookForm data = f.get();
		
		// 入力情報を元に書籍情報の登録
		if (data.amount > 0) {
			for (int i = 0; i < data.amount; i++) {
				Book book = new Book(data);
				book.group = new Group(optUser.get().groupId);
				Option<Book> optBook = bookservice.save(book);
				if (optBook.isEmpty()) {
					// 正常に登録できなかった場合は、エラー情報を返す
					result.put(CommonConstants.RETURN_JSON_KEY_RESULT,CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
					result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE,"書籍の登録に失敗しました。");
					return ok(result);
				}
				BookItem item = new BookItem(optBook.get());
				if(item.shelfId != null){
					item.shelfName = shelfService.findById(item.shelfId).get().name;
				}
				bookItemList.add(item);
			}
		}

		// 承認済み書籍のIDを保持している場合、対象の承認済み書籍を削除する。
		if (data.applyDeleteId != null) {
			ApplyService applyService = new ApplyService();
			Option<Apply> apply = applyService.findById(data.applyDeleteId);
			if (!apply.isEmpty()) {
				applyService.delete(apply.get());
			}
		}
		
		result.put(CommonConstants.RETURN_JSON_KEY_RESULT,CommonConstants.RETURN_JSON_VALUE_RESULT_COMPLETE);
		result.put("bookList", Json.toJson(bookItemList));
		result.put("userId", optUser.get().id);
		return ok(result);
	}

	/**
	 * update():書籍更新
	 *
	 * @param id
	 * @return
	 */
	@AdminCheck
	public static Result update(Integer id) {
		ObjectNode result = Json.newObject();
		BookService bookservice = new BookService();
		ShelfService shelfService = new ShelfService();
		
		// ユーザ情報取得
		Option<UserInfo> optUser = UserInfo.getInstance(session("userInfo"));
		if (optUser.isEmpty()) {
			return notFound();
		}

		// 入力情報を取得
		Form<BookForm> f = form(BookForm.class).bindFromRequest();
		BookForm data = f.get();

		// 編集対象書籍の取得
		Option<Book> bookResult = bookservice.findById(id.longValue());
		if (!bookResult.isDefined()) {
			result.put(CommonConstants.RETURN_JSON_KEY_RESULT,CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
			result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "編集対象の書籍が存在しません。");
			return ok(result);
		}
		
		// 書籍情報更新
		Book inputData = new Book(data);
		inputData.id = bookResult.get().id;
		bookResult = bookservice.update(inputData);
		if (!bookResult.isDefined()) {
			result.put(CommonConstants.RETURN_JSON_KEY_RESULT,CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
			result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE, "書籍情報の更新に失敗しました。");
			return ok(result);
		}
		BookItem item = new BookItem(bookResult.get());
		if(item.shelfId != null){
			item.shelfName = shelfService.findById(item.shelfId).get().name;
		}
		
		result.put(CommonConstants.RETURN_JSON_KEY_RESULT,CommonConstants.RETURN_JSON_VALUE_RESULT_COMPLETE);
		result.put("book", Json.toJson(item));
		result.put("userId", optUser.get().id);
		return ok(result);
	}

	/**
	 * 書籍削除機能
	 *
	 * delete()
	 *
	 * @param id
	 * @return
	 */
	@AdminCheck
	public static Result delete(Long id) {

		BookService bookService = new BookService();
		Option<Book> book = bookService.findById(id);
		ObjectNode result = Json.newObject();

		if (book.isEmpty()) {
			result.put(CommonConstants.RETURN_JSON_KEY_RESULT,
					CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
			result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE,
					"対象書籍が見つかりませんでした");
			return ok(result);
		}

		if (book.get().isRentaled()) {
			result.put(CommonConstants.RETURN_JSON_KEY_RESULT,
					CommonConstants.RETURN_JSON_VALUE_RESULT_ERROR);
			result.put(CommonConstants.RETURN_JSON_KEY_MESSAGE,
					"貸出中の書籍であるため、削除できません");
			return ok(result);
		}

		bookService.delete(book.get());
		result.put(CommonConstants.RETURN_JSON_KEY_RESULT,
				CommonConstants.RETURN_JSON_VALUE_RESULT_COMPLETE);
		return ok(result);

	}

	/**
	 * 書籍アップロード機能
	 *
	 * @param id
	 * @return
	 * @throws IOException
	 */
	@AdminCheck
	public static Result upload() throws Exception {
		// セッション情報を取得
		String json = session("userInfo");
		Option<UserInfo> opt = UserInfo.getInstance(json);
		UserInfo userInfo = opt.get();

		// CSVファイル取得
		File file = request().body().asMultipartFormData().getFiles().get(0)
				.getFile();
		if (!file.isFile()) {
			return null;
		}

		BookService bookService = new BookService();
		bookService.updateByCsv(file, userInfo.groupId);

		ObjectNode res = Json.newObject();
		res.put(CommonConstants.RETURN_JSON_KEY_RESULT,
				CommonConstants.RETURN_JSON_VALUE_RESULT_COMPLETE);
		return ok(Json.toJson(res));
	}

	/**
	 * 書籍ダウンロード機能
	 *
	 * @return
	 * @throws IOException
	 */
	@AdminCheck
	public static Result download() throws Exception {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream();) {
			// セッション情報を取得
			String json = session("userInfo");
			Option<UserInfo> opt = UserInfo.getInstance(json);
			UserInfo userInfo = opt.get();

			BookService bookService = new BookService();
			bookService.outputByCsv(baos, userInfo.groupId);

			response().setHeader("Content-Disposition",
					"attachment; fileName=BookInfo.csv");
			return ok(new ByteArrayInputStream(baos.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
