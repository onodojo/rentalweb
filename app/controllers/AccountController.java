package controllers;

import static play.data.Form.form;
import models.entity.User;
import models.form.PasswordForm;
import models.form.UserForm;
import models.response.ResultError;
import models.service.GroupService;
import models.service.UserService;
import models.session.UserInfo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.api.data.validation.ValidationError;
import play.data.Form;
import play.libs.Json;
import play.libs.F.Option;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.account;

public class AccountController extends Controller{
	public static Result index(){
		return ok(account.render(""));
	}
	
	public static Result password() throws Exception{
		Form<PasswordForm> f = form(PasswordForm.class).bindFromRequest();

    	if(f.hasErrors()){
    		// 単項目チェックエラー
    		ObjectMapper mapper = new ObjectMapper();
    		return ok(mapper.writeValueAsString(new ResultError(f.errors())));
    	}
    	
    	ObjectNode result = Json.newObject();
    	result.put("result",-1);
    	
    	// ログインユーザ情報取得
    	UserInfo userInfo = UserInfo.getInstance(session("userInfo")).get();
    	
    	// 現在のパスワードをチェック
    	User user = UserService.use().findById(userInfo.id).get();
    	if(!user.password.equals(f.get().oldPass)){
    		// エラー
    		result.put("errmsg", "現在のパスワードが異なります。");
    		return ok(result);
    	}
    	
    	if(!f.get().newPass.equals(f.get().newPassConfirm)){
    		// エラー
    		result.put("errmsg", "新しいパスワードと新しいパスワード（確認）が違います。");
    		return ok(result);
    	}
    	
    	user.password = f.get().newPass;
    	
    	// パスワード更新
    	UserService.use().update(user);
    	
    	result.put("result", 0);
		return ok(result);
	}
}
