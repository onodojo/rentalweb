package controllers;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import interceptors.AdminCheck;
import interceptors.LoginCheck;
import models.entity.Group;
import models.entity.Shelf;
import models.entity.User;
import models.service.GroupService;
import models.service.ShelfService;
import models.session.UserInfo;
import play.data.DynamicForm;
import play.libs.F.Option;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@LoginCheck
@AdminCheck
public class GroupController extends Controller {


	/**
	 * セッションに保持したグループIDをもとにグループ情報を取得し、グループ管理画面を返却する。
	 * @return グループ管理画面
	 */
	public static Result index(){
		
		// セッションからユーザ情報を取得
		Option<UserInfo> session = UserInfo.getInstance(session("userInfo"));
		UserInfo userInfo = session.get();
		
		Option<Group> group = GroupService.use().findById(userInfo.groupId);

		return ok(views.html.group.render(group.get()));
	}

	public static Result create(){
		return null;
	}

    public static Result update(Integer id) {
    	
    	JsonNode request = request().body().asJson();
    	Group group = new Group();
    	group.id = request.get("id").asLong();
    	group.name = request.get("name").asText();
    	group.anounce = request.get("anounce").asText();
    	
    	// グループ情報更新
    	Option<Group> resultGroup = GroupService.use().update(group);
    	
    	if(resultGroup.isEmpty()){
    		// error	
    	}
    	
    	Iterator<JsonNode> inputShelfList = request.get("shelfList").elements();
    	while(inputShelfList.hasNext()){
    		JsonNode inputShelf = inputShelfList.next();
    		
    		Shelf shelf = new Shelf();
    		shelf.name = inputShelf.get("name").asText();
    		shelf.id = inputShelf.get("id").asLong();
    		//shelf.kind = inputShelf.get("kind").asInt();
    		shelf.group = group;
    		
    		Option<Shelf> resultShelf;
    		if(shelf.id == 0){
    			resultShelf = ShelfService.use().save(shelf);
    		}else{
    			if(inputShelf.get("remove").asBoolean()){
        			// 削除
    				resultShelf = ShelfService.use().delete(shelf.id);
        			continue;
        		}else{
        			resultShelf = ShelfService.use().update(shelf);
        		}
    			
    		}
    		
    		if(resultShelf.isEmpty()){
    			// error
    		}
    	}
    	
    	return ok(Json.toJson(group));
    }

    public static Result deleteChk(){
    	ObjectMapper mapper = new ObjectMapper();
    	DynamicForm f = new DynamicForm();
    	String groupName = f.bindFromRequest().get("groupName");
//    	String test = mapper.writeValueAsString();
//    	 JsonNode json = request().body().asJson();

		// セッション情報の取得
    	String json = session("userInfo");
    	Option<UserInfo> opt = UserInfo.getInstance(json);

    	if(opt.isEmpty()){
    		return notFound();
    	}

    	UserInfo userInfo = opt.get();
    	Option<Group> optGroupList = GroupService.use().findById(userInfo.groupId);
    	if(optGroupList.isEmpty()){
    		//error
    		return notFound();
    	}

    	Group group = optGroupList.get();
		if(!groupName.equals(group.name)){
			//error
			return notFound();

		}

		for(User user : group.userList){
			if(user.id == userInfo.id){
				//ok
				delete(userInfo.id);
		        return ok("できた");

			}
		}

//    	if(optGroupList.isDefined()){
//    		Group group = optGroupList.get();
//    		if(!groupName.equals(group.name)){
//    			//error
//
//    		}
//
//    		for(User user : group.userList){
//    			if(user.id == userInfo.id){
//    				//ok
//    			}
//    		}
//    		//error
//    		//グループIDに紐付くユーザリストを取得
////    		List<Group> groupList = Group.find.where().eq("id", userInfo.groupId).join("m_u.findUnique();
////
////    		return ok("");
//    	}

        return ok("");
    }

    public static Result delete(Long id){
        GroupService groupService = new GroupService();
        Option<Group> group = groupService.deleteByGroupId(id.longValue());
        ObjectNode result = Json.newObject();

        if(group.isEmpty()){
        	result.put("msg","ない" );
        	return ok(result);
        }

		result.put("msg", "削除成功");
        return ok(result);
    }
    public static Result deletechk(){
       return null;
    	// return ok(deletegroup.render());
    }

}
