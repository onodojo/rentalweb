package utils;

import java.sql.Date;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;

public class StringUtil extends StringUtils{
	
	public static String parseDateToString(Date date){

        String strDate = new SimpleDateFormat(CommonConstants.DATE_FORMAT_YYYYMMDD_HH_MM_SS).format(date);
        
		return strDate;
	}
}
