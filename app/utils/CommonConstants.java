package utils;

public class CommonConstants {

	public static final String DATE_FORMAT_YYYYMMDD_HH_MM_SS = "yyyyMMdd";
	public static final String STRING_TO_DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";

	public static final String RETURN_JSON_KEY_RESULT = "result";
	public static final String RETURN_JSON_KEY_ERROR = "error";
	public static final String RETURN_JSON_KEY_MESSAGE = "msg";

	/** 処理結果[成功] */
	public static final int RETURN_JSON_VALUE_RESULT_COMPLETE = 0;
	/** 処理結果[失敗] */
	public static final int RETURN_JSON_VALUE_RESULT_ERROR = 1;

	/** 権限[管理者] */
	public static final String ADMIN_AUTHORITY = "0";
	/** 権限[利用者] */
	public static final String USER_AUTHORITY = "1";

	public static final String APPLY_STATE_PENDING = "0";
	public static final String APPLY_STATE_AUTHORIZED = "1";
	public static final String APPLY_STATE_WITHDRAWAL = "2";

	/** 一括登録利用グループID */
	public static final Long COLLECT_GROUP_ID = 1l;
	/** デーコムメールアドレスドメイン */
	public static final String DCOM_MAILADDRESS_DOMAIN = "@dcom-web.co.jp";
	/** 月報Web退職フラグ[在職者] */
	public static final Long GEPPOU_USER_INCUMBENT = 0l;
	/** 月報Web退職フラグ[退職者] */
	public static final Long GEPPOU_USER_RETIREMENT = 1l;

	/** 区切り文字[ハイフン] */
	public static final String SEP_HYPHEN = "-";

	/** 文字コード[Shift-JIS] */
	public static final String CHARSET_SHIFT_JIS = "Shift-JIS";
	/** 文字コード[UTF-8] */
	public static final String CHARSET_UTF8 = "UTF-8";
}
