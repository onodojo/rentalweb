package utils;

import play.Play;

public class ConfUtil {
	public static String get(String key) {
		return Play.application().configuration().getString(key);
	}
}
