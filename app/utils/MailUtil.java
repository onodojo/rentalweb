package utils;

import java.util.ArrayList;
import java.util.List;

import play.libs.F.Option;
import models.entity.User;
import models.service.UserService;

import com.typesafe.plugin.MailerAPI;
import com.typesafe.plugin.MailerPlugin;

public class MailUtil {
	
	public static final String SUBJECT_REQUEST = "【申請】";
	public static final String SUBJECT_CANCEL = "【取下】";
	public static final String SUBJECT_RENTAL = "【貸出】";
	public static final String SUBJECT_RETURN = "【返却】";
	public static final String SUBJECT_APPROVE = "【承認】";
	
	/**
	 * 管理者にメールを送信します。
	 * @param subject タイトル
	 * @param groupId グループID
	 * @param content 本文
	 */
	public static void send(String subject,Long groupId,String content){
		
		// 管理者のユーザを取得
		Option<List<User>> users = UserService.use().findByGroupId4Admin(groupId);
		
		// 送信先メールアドレス設定
		List<String> toList = new ArrayList<>();
		for(User user : users.get()){
			toList.add(user.mailaddress);
		}
		
		MailSender sender = new MailSender(subject,content,toList.toArray(new String[toList.size()]));
		sender.start();
	}
	
	/**
	 * 利用者にメールを送信します。
	 * @param subject タイトル
	 * @param userId 送信先ユーザID
	 * @param content 本文
	 */
	public static void send2User(String subject,Long userId,String content){
		
		Option<User> user = UserService.use().findById(userId);
		
		MailSender sender = new MailSender(subject,content,new String[]{user.get().mailaddress});
		sender.start();
	}
	
	/**
	 * メール送信用スレッドクラス
	 * @author h-ono
	 *
	 */
	public static class MailSender extends Thread {
		private String subject;
		private String content;
		private String[] recipient;
		
		public MailSender(String subject,String content,String[] recipient){
			this.subject = subject;
			this.content = content;
			this.recipient = recipient;
		}
		
		/**
		 * メールを送信します。
		 */
		public void run(){
			MailerAPI mail = play.Play.application().plugin(MailerPlugin.class).email();
			mail.setSubject(subject);
	    	// 送信相手のアドレス
	    	mail.setRecipient(recipient);
	    	// 送信元のアドレス
	    	mail.setFrom("kanribu@dcom-web.co.jp");
	    	// メールの内容
	    	mail.send(content);
		}
	}
}
