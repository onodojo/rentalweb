package utils;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtil {
	
	/**
	 * 現在日を取得します。
	 * 
	 * @return
	 */
	public static Date getSystemDate(){
		return Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo")).getTime();
	}
	
	/**
	 * 文字列(yyyyMMdd)を日付に変換します。
	 * 
	 * @param strDate
	 * @return
	 */
	public static Date paseStringToDate(String strDate){
		SimpleDateFormat sdf = new SimpleDateFormat(CommonConstants.DATE_FORMAT_YYYYMMDD_HH_MM_SS);
		Date date = null;
		try {
			date = (Date) sdf.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	/**
	 * 日付を文字列(yyyy-MM-dd)に変換します。
	 * 
	 * @param date
	 * @return
	 */
	public static String paseDateToString(Date date){
		if(date == null) {
			return null;
		} else {
			return new SimpleDateFormat(CommonConstants.STRING_TO_DATE_FORMAT_YYYY_MM_DD).format(date);
		}
	}
	
	/**
	 * 2つの日付の前後関係を比較した結果を返却します。<BR>
	 * 
	 * @param beforeDate
	 * @param afterDate
	 * @return
	 */
	public static boolean compareToDate(Date beforeDate,Date afterDate){
		Calendar beforeCal = parseDateToCalendarDate(beforeDate);
		Calendar afterCal = parseDateToCalendarDate(afterDate);
		return beforeCal.equals(afterCal) || beforeCal.before(afterCal);
	}
	
	/**
	 * Date型の日付をCalendar型に変換する。<BR>
	 * 時間は0で設定する。
	 * 
	 * @param date
	 * @return
	 */
	public static Calendar parseDateToCalendarDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}
}
