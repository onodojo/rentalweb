/**
 * アカウント設定画面用javascript
 */

$(function(){
	$('#btnSavePassword').click(savePassword);
});

// パスワードを保存します
var savePassword = function(){
	$.ajax({
        type     : 'PUT',
        url      : common.getAppRoot() + '/account/password',
        data     : $('#frmPassword').serialize(),
        dataType : 'json',
        cache    : false,
        success  : function(data, status, jqXHR) {
        	//エラーチェック
        	if(data.result == -1){
        		if(data.errors){
	        		for(var i = 0;i < data.errors.length;i++){
	        			$('p.' + data.errors[i].name).text(data.errors[i].message);
	        		}
        		}
        		
        		if(data.errmsg){
        			$("#errmsg").text(data.errmsg);
        		}
        		
        		return;
        	}
        	
        	common.notifySuccess('保存しました');
        },
        error: function(jqXHR, status, error) {
        	common.notifyError("error");
        }
    });
};