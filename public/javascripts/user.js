/**
 * ユーザー画面用javascript
 */

//画面読み込み時処理(サイドメニュー対応)
$(window).load(function(){
	$(".menulink").removeClass("active");
	$(".menulink.user").addClass("active");
});

var oTable;
var targetRow;

$(function () {
	// モーダルダイアログの初期化
	initModal();
	
	// ユーザ一覧表示
	getUserAll(function(){
		oTable = $("#example1").dataTable({
			"oLanguage": {
				"sProcessing": "処理中...",
				"sLengthMenu": "表示件数 _MENU_ 件",
				"sZeroRecords": "データはありません",
				"oPaginate": {
					"sNext": "次のページ",
					"sPrevious": "前のページ"
				},
				"sLoadingRecords": "読み込み中...",
				"sEmptyTable": "データはありません",
				"sInfo": "全_TOTAL_件中から_END_件を表示",
				"sInfoEmpty": "全0件中から0件を表示",
				"sInfoFiltered": "(全_MAX_件から検索)",
				"sSearch": "検索："
			}
		});
		oTable.fnSort([1, 'desc']);
	});

	$('#user').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true
	});

	// 編集モーダル保存ボタンクリック時
	$("#btnEditSave").click(function () {
		$(this).prop("disabled", true);
		save();
	});

	$("#btnExecSearch").click(function () {
		$("#conditionArea").empty();
		$("#modalSearchForm").find("input[type='text']").each(function () {
			if ($(this).val() !== '') {
				var label = $(this).closest("div").find("label").text();
				$("#conditionArea").append($("<span class='label label-info'></span>").text(label + ":" + $(this).val())).append("&nbsp;");
			}
		});
		$("#modalSearchForm").modal("hide");
	});

	/**
	 * テーブルデータ選択イベント
	 */
	$('#example1 tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
			targetRow = undefined;
			$('#btnEdit,#btnDelete').attr('disabled','disabled');
		} else {
			oTable.$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
			targetRow = $(this);
			$('#btnEdit,#btnDelete').removeAttr('disabled');
		}
	});
	
	/**
	 * リストボックスの選択イベント
	 */
	$("#selectAction").change(function() {
		var selectId = $("#selectAction option:selected").attr("id");
		if(selectId == 'btnAdd'){
			createUser();
		}else if(selectId == 'btnEdit'){
			editUser();
		}else if(selectId == 'btnDelete'){
			deleteUser();
		}else if(selectId == 'btnDownload'){
			downloadUser();
		}else if(selectId == 'btnOpenUpload'){
			uploadUser();
		}
		$("#selectAction option").attr("selected", false);
	});
	
	/**
	 * ユーザ一括登録(オプション)
	 */
	$('#btnCollectUpload').on('click',function(){
		$(this).prop("disabled", true);
		collectUpload();
	});
});

/**
 * ユーザ情報登録
 */
var createUser = function(){
	$("#frmEdit").find("input[name='id']").val("");
	$("#editUserName").val("");
	$("#editMail").val("");
	$('#editPassword').val("");
	$('#editAuthority2').prop("checked",true);
	$('input').next('p').text('');
	$("#modalEditForm").modal('show');
};

/**
 * ユーザ情報編集
 */
var editUser = function(){
	$.ajax({
		url:common.getAppRoot() + "/user/" + targetRow.attr("data-id"),
		method:"GET",
		success:function(res){
			$('#frmEdit').deserialize($.evalJSON(res));
		},
		error:function(){
			common.notifyError("error!");
		}
	});
	$('input').next('p').text('');
	$("#modalEditForm").modal('show');
};

/**
 * ユーザ情報削除
 */
var deleteUser = function(){
	if (window.confirm("削除します。よろしいですか？")) {
		//DB上削除処理
		//非同期通信
		$.ajax({
			type     : 'delete',
			url      : common.getAppRoot() + '/user/' + targetRow.attr("data-id"),
			dataType : 'json',
			cache    : false,
			success  : function(data, status, jqXHR) {
				//画面上削除処理
				if(data['flg']=="1"){
					oTable.fnDeleteRow(targetRow.closest("tr"))
					common.notifySuccess('削除しました');
				};
			},
			error: function(jqXHR, status, error) {
				common.notifyError('ユーザの削除に失敗しました。');
			}
		});
	}
};

/**
 * csvダウンロード
 */
var downloadUser = function(){
	location.href = common.getAppRoot() + "/user/download";
};

/**
 * csvアップロード
 */
var uploadUser = function(){
	$("#modalUpload").modal("show");
};

/**
 * ユーザ一括登録(オプション)
 */
var collectUpload = function(){
	$.ajax({
		url:common.getAppRoot() + '/user/collect/upload',
		method:'POST',
		success:function(res){
			// 一覧再描画処理
			refreshDisplay('ユーザの更新が完了しました。');
		},
		error:function(){
			common.notifyError('ユーザの更新に失敗しました。');
		},
		complete:function(){
			$('#btnCollectUpload').prop('disabled', false);
		}
	});
};

/**
 * ユーザ一覧再描画処理
 */
var refreshDisplay = function(msg){
	$.ajax({
		type     : 'get',
		url      : common.getAppRoot() + '/user/list',
		dataType : 'json',
		cache    : false,
		success  : function(data, status, jqXHR) {
			// テーブルデータリセット
			oTable.fnClearTable();
			
			for (i = 0; i < data.length; i++) {
				createUserData(data[i]);
			}
			common.notifySuccess(msg);
		},
		error: function(jqXHR, status, error) {
			common.notifyError('通信に失敗');
		}
	});
}

/**
 * ユーザ情報の保存を行います。
 */
var save = function(){
	// フォームを取得
	var $form = $('#frmEdit');
	var param = {};

	// フォームの内容を取得
	$($form.serializeArray()).each(function(i, v) {
		param[v.name] = v.value;
	});

	var id = $form.find("input[name='id']").val();

	if(id == ""){
		saveRequest(common.getAppRoot() + "/user","POST",param,function(data){
			putUserList(data);
		});
	}else{
		saveRequest(common.getAppRoot() + "/user/" + id,"PUT",param,function(data){
			targetRow.find("td.userName").text(data.name);
			targetRow.find("td.mail").text(data.mailaddress);
			targetRow.find("td.authority").text(data.authority);
		});
	}
};

/**
 * 入力データを送信します。
 */
var saveRequest = function(url,method,param,successFunc){
	$.ajax({
		type     : method,
		url      : url,
		data     : param,
		dataType : 'json',
		cache    : false,
		success  : function(data, status, jqXHR) {
			//エラーチェック
			if(data.result == -1){
				common.notifyError('入力エラー');
				for(var i = 0;i < data.errors.length;i++){
					$('p.' + data.errors[i].name).text(data.errors[i].message);
				}
				$("#btnEditSave").prop("disabled", false);
				return;
			}
			common.notifySuccess('保存しました');
			successFunc(data);
			$("#modalEditForm").modal('hide');
			$("#btnEditSave").prop("disabled", false);
		},
		error: function(jqXHR, status, error) {
			common.notifyError('通信に失敗');
			$("#btnEditSave").prop("disabled", false);
		}
	});
};

/**
 * ユーザリスト反映
 * 引数のユーザデータをリストの末尾に追加する。
 *
 * memodata(JSON)
 * {id:"ユーザID",
 * 	empNo:"従業員番号",
 * 	userName:"ユーザ名",
 * 	mail:"メールアドレス",
 * 	loginId:"ログインID",
 * 	authority:"権限"}
 *
 */
var putUserList = function(userdata){
	//リストに追加するメモ情報の作成
	var putuser = '<tr  data-id=';
	putuser += userdata['id'];
	putuser += '>';
	putuser += ' <td class="userName">';
	putuser += userdata['name'];
	putuser += ' </td>';
	putuser += ' <td class="mail">';
	putuser += userdata['mailaddress'];
	putuser += ' </td>';
	putuser += ' <td class="authority">';
	putuser += userdata['authority'];
	putuser += ' </td>';
	putuser += '</tr>';

	// 行追加
	$("#example1").DataTable().row.add($(putuser)).draw();
};

/**
 * ユーザ一覧表示
 */
var getUserAll = function(fn){
	//非同期通信
	$.ajax({
		type     : 'get',
		url      : common.getAppRoot() + '/user/list',
		dataType : 'json',
		cache    : false,
		success  : function(data, status, jqXHR) {
			fn();
			for (i = 0; i < data.length; i++) {
				createUserData(data[i]);
			}
		},
		error: function(jqXHR, status, error) {
			common.notifyError('通信に失敗');
		}
	});
}

/**
 * ユーザ一覧テーブル行生成
 */
var createUserData = function(user){
	// 行オブジェクトの準備
	var $tr = $('#template tr').clone();
	$tr.attr('data-id',user['id']);
	$tr.find('td.userName').text(user['name']);
	$tr.find('td.mail').text(user['mailaddress']);
	$tr.find('td.authority').text(user['authority']);
	oTable.fnAddData($tr);
}

/**
 * モーダルダイアログの初期化
 */
var initModal = function(){
	$.ajax({
		url:common.getAppRoot() + "/assets/modals/upload.modal.html",
		success:function(html){
			$("body").append(html);
			$('#modalUpload').attr('data-url','user/upload');
		}
	});
}
