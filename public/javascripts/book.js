/**
 * 書籍一覧画面
 */

common.getAppRoot();

var oTable, oTable1;
var targetRow;
var lendTargetRow;
var returnTargetRow;

//画面読み込み時処理(サイドメニュー対応)
$(window).load(function(){
	$(".menulink").removeClass("active");
	$(".menulink.book").addClass("active");
});

$(function() {
	
	// モーダル初期化
	initModal();
	
	// ポップオーバー設定
	$('.bookName').each(function(){
		$(this).find('span:eq(0)').popover({
			placement: 'right',
			content: setDetail($(this).find('div.detail')),
			html: true,
			trigger: 'hover'
		});
	});
	
	// テーブル初期化
	initTable();
	
	// 絞り込みボタンクリック時
//	$("#btnSearch").click(function() {
//		$("#modalSearchForm").modal('show');
//	});
//	$("#btnExecSearch").click(function() {
//		$("#conditionArea").empty();
//		$("#modalSearchForm").find("input[type='text']").each(function() {
//			if ($(this).val() !== '') {
//				var label = $(this).closest("div").find("label").text();
//				$("#conditionArea").append($("<span class='label label-info'></span>")
//						.text(label+ ":"+ $(this).val())).append("&nbsp;");
//			}
//		});
//		$("#modalSearchForm").modal("hide");
//	});

	// 行選択イベント
	$('#example1 tbody').on('click', 'tr', function() {
		if ($(this).find('td').hasClass('dataTables_empty')) {
			return;
		}
		if ($(this).hasClass('selected')) {
			$(this).removeClass('selected');
			targetRow = undefined;
			$("#btnEdit").attr('disabled','disabled');
			$("#btnDelete").attr('disabled','disabled');
			$("#btnHistory").attr('disabled','disabled');
		} else {
			oTable.$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
			targetRow = $(this);
			$("#btnEdit").removeAttr('disabled');
			$("#btnDelete").removeAttr('disabled');
			$("#btnHistory").removeAttr('disabled');
		}
	});

//	$("#tag").tagit();

	// 絞り込みイベント
	$('input[name="options"]:radio').change(function(){
		//非同期通信
		$.ajax({
			type     : 'GET',
			url      : common.getAppRoot() + '/book/search/' + $(this).val(),
			cache    : false,
			success  : function(data, status, jqXHR) {
				// テーブルデータリセット
				oTable.fnClearTable();
				
				// 一覧再描画処理
				for (i = 0; i < data.bookList.length; i++) {
					var bookdata = data.bookList[i];
					//テーブルに追加
					addBookData(bookdata,data.userId);
				}
			},
			error: function(jqXHR, status, error) {
				common.notifyError('通信に失敗');
			}
		});
	});

	/*
	 * リストボックスの選択イベント
	 */
	$("#selectAction").change(function() {
		var selectId = $("#selectAction option:selected").attr("id");
		if(selectId == 'btnAdd'){
			createBook();
		}else if(selectId == 'btnEdit'){
			editBook();
		}else if(selectId == 'btnDelete'){
			deleteBook();
		}else if(selectId == 'btnHistory'){
			historyBook();
		}else if(selectId == 'btnDownload'){
			downloadBook();
		}else if(selectId == 'btnUpload'){
			uploadBook();
		}
		$("#selectAction option").attr("selected", false);
	});

	// 削除
	var deleteBook = function(){
		if (window.confirm("削除します。よろしいですか？")) {
			//非同期通信
			$.ajax({
				type     : 'DELETE',
				url      : common.getAppRoot() + '/book/' + targetRow.attr("data-id"),
				cache    : false,
				success  : function(data, status, jqXHR) {
					oTable.fnDeleteRow(targetRow);
					common.notifySuccess('削除に成功しました');
				},
				error: function(jqXHR, status, error) {
					common.notifyError('通信に失敗');
				}
			});
		}
	}

	// 履歴
	var historyBook = function(){
		//非同期通信
		$.ajax({
			type     : 'GET',
			url      : common.getAppRoot() + '/rental/history/' + targetRow.attr("data-id"),
			cache    : false,
			success  : function(data, status, jqXHR) {
				$("#example2 tbody").empty();
				for (i = 0; i < data.length; i++) {
					var history = data[i];
					//テーブルに追加
					var tr = '<tr><td class="user">'+ history["rentalUserName"] +'</td>';
					tr = tr + '<td class="lendDate">'+ history["rentalDate"] +'</td>';
					tr = tr + '<td class="returnDate">'+ history["returnDate"] +'</td>';
					tr = tr + '</tr>';
					$("#example2 tbody").append(tr);
				}
				$("#modalHistory").modal("show");
			},
			error: function(jqXHR, status, error) {
				common.notifyError('通信に失敗');
			}
		});
	};

	// ダウンロード
	var downloadBook = function(){
		location.href = common.getAppRoot() + "/book/download";
	}

	// アップロード
	var uploadBook = function(){
		$("#modalUpload").modal("show");
	}

	// 貸出ボタンクリック
	$(document).on('click', 'button.btnLend', function() {
		var $tr = $(this).closest('tr');
		lendTargetRow = $(this).closest('tr');

		$('#rental-id').val($tr.data("id"));
		$('#lendIsbn').text($tr.find('td.isbn').text());
		$('#lendBookName').text($tr.find('td.bookName').text());
		$('#lendPublisher').text($tr.find('td.publisher').text());
		$('#lendAuthor').text($tr.find('td.author').text());
		$('#lendDevice').text($tr.find('td.place').text());
		if ($tr.find('td.isbn').find("i").hasClass("fa-tablet")) {
			$("#rentalModal").find("div.device").removeClass("hide");
		} else {
			$("#rentalModal").find("div.device").addClass("hide");
		}
		$("#returnPlanDate").val("")
		$("#rentalDate").val("")
		$('#rentalModal').modal('show');
	});

	// 返却ボタンクリック
	$(document).on('click', 'button.btnReturn', function() {
		var $tr = $(this).closest('tr');
		returnTargetRow = $(this).closest('tr');
		var $modal = $("#modalReturnForm");

		$modal.find("#return-id").val($tr.data("id"));
		$modal.find("div.isbn").text($tr.find('td.isbn').text());
		$modal.find("div.bookName").text($tr.find('td.bookName').text());
		$modal.find("div.publisher").text($tr.find('td.publisher').text());
		$modal.find("div.author").text($tr.find('td.author').text());
		$modal.find("div.place").text($tr.find('td.place').text());

		if ($tr.find('td.isbn').find("i").hasClass("fa-tablet")) {
			$modal.find("div.checkbox").removeClass("hide");
		} else {
			$modal.find("div.checkbox").addClass("hide");
		}

		$modal.modal('show');
	});
});

// モーダルダイアログの初期化
var initModal = function(){
	$.ajax({
		url:common.getAppRoot() + "/assets/modals/book/create.modal.html",
		success:function(html){
			$("body").append(html);
			initCreateModal();
		}
	});

	$.ajax({
		url:common.getAppRoot() + "/assets/modals/book/history.modal.html",
		success:function(html){
			$("body").append(html);
		}
	});

	$.ajax({
		url:common.getAppRoot() + "/assets/modals/book/rental.modal.html",
		success:function(html){
			$("body").append(html);
		}
	});

	$.ajax({
		url:common.getAppRoot() + "/assets/modals/book/return.modal.html",
		success:function(html){
			$("body").append(html);
		}
	});

	$.ajax({
		url:common.getAppRoot() + "/assets/modals/book/search.modal.html",
		success:function(html){
			$("body").append(html);
		}
	});

	$.ajax({
		url:common.getAppRoot() + "/assets/modals/upload.modal.html",
		success:function(html){
			$("body").append(html);
			$('#modalUpload').attr('data-url','/book/upload');
		}
	});

	$.ajax({
		url:common.getAppRoot() + "/assets/modals/book/apply.modal.html",
		success:function(html){
			$("body").append(html);
		}
	});
}

// 書籍登録・編集モーダルイベント
var initCreateModal = function(){
	// 【クリックイベント】自動取得
	$("#btnAutoAcqire").click(function() {
		findBookInfoByIsbn();
	});
	// 【クリックイベント】保存
	$("#btnSave").click(function() {
		bookSave();
	});
	// 【クリックイベント】承認済み書籍から選ぶ
	$("#applyBook").click(function() {
		applyBook();
	});
};

// ISBN書籍情報取得
var findBookInfoByIsbn = function(){
	var isbn = formatIsbn($("#isbn").val());
	if(isbn === ''){
		$('p.isbn').text('isbnを再入力してください。');
		return;
	}
	$("#isbn").val(isbn)
	$('p.isbn').text('');
	bookInfo(isbn);
};

// ISBN整形
var formatIsbn = function(input){
	var isbn = input.replace(/-/g, '');
	if(isbn.length === 13){
		return convert13to10(isbn);
	}else if(isbn.length === 10){
		return isbn;
	}else{
		return '';
	}
};

// 13桁ISBN変換
var convert13to10 = function(isbn13){
	isbn13 += "";
	var digits = [];
	digits = isbn13.substr(3,9).split("") ;
	var sum = 0;
	var chk_tmp, chk_digit;
	for(var i = 0; i < 9; i++) {
		sum += digits[i] * (10 - i);
	}
	chk_tmp= 11 - (sum % 11);
	if (chk_tmp == 10) {
		chk_digit = 'x';
	} else if (chk_tmp == 11) {
		 chk_digit = 0;
	} else {
		chk_digit = chk_tmp;
	}
	digits.push(chk_digit);
	return digits.join("");
}

// 書籍情報ISBN検索
var bookInfo = function(isbn){
	//非同期通信
	$.ajax({
		type     : 'GET',
		url      : 'https://www.googleapis.com/books/v1/volumes?q=' + isbn ,
		cache    : false,
		data_type: 'jsonp',
		success  : function(data, status, jqXHR) {
			var items = data['items'];
			if(items){
				$("#bookName").val(items[0]['volumeInfo']['title']);
				$("#author").val(items[0]['volumeInfo']['authors']);
				$("#publisher").val(items[0]['volumeInfo']['publisher']);
				$("#url").val(items[0]['volumeInfo']['infoLink']);
			}else{
				$('p.isbn').text('対象の書籍がみつかりません。');
				$("#bookName").val('');
				$("#author").val('');
				$("#publisher").val('');
				$("#url").val('');
			}
		},
		error: function(jqXHR, status, error) {
			common.notifyError('通信に失敗');
		}
	});
};

//書籍登録・編集情報保存処理
var bookSave = function(){
	var type = $("#eventType").val();
	if(type == "create"){
		createSave();
	}else if(type == "edit"){
		editSave();
	}else{
		common.notifyError('システムエラー');
	}
};

// 書籍追加
var createSave = function() {
	$.ajax({
		type     : 'POST',
		url      : common.getAppRoot() + '/book',
		data     : initSaveBookData(),
		dataType : 'json',
		cache    : false,
		success  : function(data, status, jqXHR) {
			// 登録処理の結果を判定
			if(data.result !== 0){
				common.notifyError(data.msg);
				return;
			}
			
			// 登録した書籍情報をテーブルに追加
			for (i = 0; i < data.bookList.length; i++) {
				var bookdata = data.bookList[i];
				addBookData(bookdata,data.userId);
			}
			
			common.notifySuccess('登録に成功しました');
			$("#createModal").modal('hide');
		},
		error: function(jqXHR, status, error) {
			common.notifyError('通信に失敗');
			$("#createModal").modal('hide');
		}
	});
};

// 書籍編集
var editSave = function() {
	$.ajax({
		type     : 'PUT',
		url      : common.getAppRoot() + '/book/'+targetRow.data("id"),
		data     : initSaveBookData(),
		dataType : 'json',
		cache    : false,
		success  : function(data, status, jqXHR) {
			// 登録処理の結果を判定
			if(data.result !== 0){
				common.notifyError(data.msg);
				return;
			}
			
			// 更新対象行リセット
			oTable.fnDeleteRow(targetRow);
			
			//テーブルを更新
			addBookData(data.book,data.userId);
			
			common.notifySuccess('編集に成功しました');
			$("#createModal").modal('hide');
		},
		error: function(jqXHR, status, error) {
			common.notifyError('通信に失敗');
			$("#createModal").modal('hide');
		}
	});
};

var initSaveBookData = function(){
	return {
			"isbn" : $("#isbn").val(),
			"name" : $("#bookName").val(),
			"url" : $("#url").val(),
			"publisher" : $("#publisher").val(),
			"author" : $("#author").val(),
			"shelfId" : $("#shelfList option:selected").val(),
			"bookType" : $("#shelfList option:selected").data("kind"),
			"recommend": $("[name=recommend]:checked").val(),
			"amount": $("#amount").val(),
			"applyDeleteId": $("#applyDeleteId").val()
	};
};

// 承認書籍選択ダイアログ表示
var applyBook = function(){
	// 初期化
	$("#applyTable tbody *").remove();
	
	//非同期通信
	$.ajax({
		type     : 'GET',
		url      : common.getAppRoot() + '/apply/approval',
		cache    : false,
		success  : function(data, status, jqXHR) {
			for (i = 0; i < data.length; i++) {
				var applyBook = data[i];
				//テーブルに追加
				var tr = '<tr data-id="'+ applyBook["id"] +'"><td><button type="button" class="btn btn-flat btn-default btn-select-apply-book">選択</button></td>';
				tr = tr + '<td class="user">'+ applyBook["user"] +'</td>';
				tr = tr + '<td class="applyDate">'+ applyBook["applyDate"] +'</td>';
				tr = tr + '<td class="requestDate">'+ applyBook["authorizeDate"] +'</td>';
				tr = tr + '<td class="isbn">'+ applyBook["isbn"] +'</td>';
				if(applyBook["booktype"] == 1){
					tr = tr + '<td class="type" data-id="'+applyBook["booktype"]+'">紙</td>';
				}else{
					tr = tr + '<td class="type" data-id="'+applyBook["booktype"]+'">電子</td>';
				}
				tr = tr + '</tr>';
				$("#applyTable tbody").append(tr);
			}
			$("#modalApply").modal('show');
		},
		error: function(jqXHR, status, error) {
			common.notifyError('通信に失敗');
		}
	});
};

//データテーブル設定
var initTable = function(){
	// 書籍テーブル
	oTable = $("#example1").dataTable({
		"oLanguage" : {
			"sProcessing" : "処理中...",
			"sLengthMenu" : "表示件数 _MENU_ 件",
			"sZeroRecords" : "データはありません",
			"oPaginate" : {
				"sNext" : "次のページ",
				"sPrevious" : "前のページ"
			},
			"sLoadingRecords" : "読み込み中...",
			"sEmptyTable" : "データはありません",
			"sInfo" : "全_TOTAL_件中から_END_件を表示",
			"sInfoEmpty" : "全0件中から0件を表示",
			"sInfoFiltered" : "(全_MAX_件から検索)",
			"sSearch" : "検索："
		},
		"aoColumnDefs" : [ {
			"sWidth" : "50px",
			"aTargets" : [ 5 ]
		},{
			"sWidth" : "80px",
			"aTargets" : [ 1,2,3,4 ]
		}, {
			"bSortable" : false,
			"aTargets" : [ 5 ]
		} ]
	});

	// 貸出履歴テーブル
	oTable1 = $("#example2").dataTable({
		"oLanguage" : {
			"sProcessing" : "処理中...",
			"sLengthMenu" : "表示件数 _MENU_ 件",
			"sZeroRecords" : "データはありません",
			"oPaginate" : {
				"sNext" : "次のページ",
				"sPrevious" : "前のページ"
			},
			"sLoadingRecords" : "読み込み中...",
			"sEmptyTable" : "データはありません",
			"sInfo" : "全_TOTAL_件中から_END_件を表示",
			"sInfoEmpty" : "全0件中から0件を表示",
			"sInfoFiltered" : "(全_MAX_件から検索)",
			"sSearch" : "検索："
		}
	});

	oTable.fnSort([ 1, 'desc' ]);
	oTable1.fnSort([ 1, 'desc' ]);
};

// ポップオーバー設定
var setDetail = function (detail){
	var $table = $('#detailTemplate table').clone();
	$table.find('td.isbn').text($(detail).find('div.isbn').text());
	$table.find('td.publisher').text($(detail).find('div.publisher').text());
	$table.find('td.author').text($(detail).find('div.author').text());
	return $table;
};

/*
 * 書籍テーブルデータ追加
 */
var addBookData = function(bookdata,userId){
	// 行オブジェクトの準備
	var $tr = $('#cloneBook tr').clone();
	$tr.attr('data-id',bookdata['id']);
	
	// 書籍名
	$tr.find('td.bookName').attr('data-recommend',bookdata['recommend']);
	$tr.find('td.bookName').attr('data-name',bookdata['name']);
	if(bookdata['url'] === null || bookdata['url'] === ''){
		$tr.find('td.bookName span:eq(0) a').remove();
		$tr.find('td.bookName span:eq(0)').text(bookdata['name']);
	}else{
		$tr.find('td.bookName span:eq(0) a i').before(bookdata['name']);
		$tr.find('td.bookName span:eq(0) a').attr('href', bookdata['url']);
	}
	if(bookdata['recommend'] == '1') {
		$tr.find('td.bookName span.label').removeClass('hide');
	}
	
	// ポップオーバー表示情報設定
	$tr.find('td.bookName div.detail div.isbn').text(bookdata['isbn']);
	$tr.find('td.bookName div.detail div.publisher').text(bookdata['publisher']);
	$tr.find('td.bookName div.detail div.author').text(bookdata['author']);
	
	// ポップオーバー設定
	$tr.find('td.bookName span:eq(0)').popover({
		placement: 'right',
		content: setDetail($tr.find('td.bookName div.detail')),
		html: true,
		trigger: 'hover'
	});
	
	// 場所/機器
	$tr.find('td.place').attr('data-id',bookdata['shelfId']);
	$tr.find('td.place').attr('data-kind',bookdata['bookType']);
	$tr.find('td.place').text(bookdata['shelfName']);
	
	// 貸出中書籍の場合別処理
	if(bookdata['rentalDate'] !== null){
		$tr.find('td.lendDate').text(bookdata['rentalDate']);
		$tr.find('td.returnPlanDate').text($('#returnPlanDate').val());
		$tr.find('td.user').text(bookdata['userName']);
		if(userId === bookdata['rentalUserId']){
			// 自分が貸出中
			$tr.find('td.status button.btnLend').addClass('hide');
			$tr.find('td.status button.btnReturn').attr('data-id',bookdata['rentalId']);
			$tr.find('td.status button.btnReturn').removeClass('hide');
		}else{
			// 他ユーザが貸出中
			$tr.find('td.status button.btnLend').addClass('hide');
			$tr.find('td.status span.on-loan').removeClass('hide');
		}
	}
	
	// 対象書籍が電子書籍の場合ボタンを非表示
	if(bookdata['shelfId'] === null){
		$tr.find('td.status button').addClass('hide');
	}
	oTable.fnAddData($tr);
};

// 追加
var createBook = function(){
	// 承認済み書籍選択リンク表示
	$("#applyBook").show();
	// 入力欄設定
	setBookModalInput('','','','','');
	// おすすめ
	$("#recommend").prop("checked", false);
	// 保管場所
	$("#shelfList").append($("#tmpShelfList").children("option"));
	$("#shelfList option").attr("selected", false);
	// 数量表示
	$("#amount").val("1");
	$("div.amount").show();
	// イベントタイプ
	$("#eventType").val("create");
	// 承認済み書籍IDリセット
	$("#applyDeleteId").val("");
	// ダイアログ表示
	$("#createModal").modal('show');
};

// 編集
var editBook = function(){
	// 承認済み書籍選択リンク非表示
	$("#applyBook").hide();
	// 入力欄設定
	setBookModalInput($.trim(targetRow.find("td.bookName div.detail div.isbn").text())
					,targetRow.find("td.bookName").data('name')
					,targetRow.find("td.bookName a").attr("href")
					,targetRow.find("td.bookName div.detail div.publisher").text()
					,targetRow.find("td.bookName div.detail div.author").text());
	// おすすめ
	if(targetRow.find("td.bookName").data("recommend") == 1){
		$("#recommend").prop("checked", true);
	}else{
		$("#recommend").prop("checked", false);
	}
	// 保管場所
	$("#shelfList").append($("#tmpShelfList").children("option"));
	$("#shelfList").val(targetRow.find("td.place").data("id"));
	// 数量非表示
	$("div.amount").hide();
	// イベントタイプ
	$("#eventType").val("edit");
	// ダイアログ表示
	$("#createModal").modal('show');
}

// 書籍ダイアログ入力欄設定
var setBookModalInput = function(isbn,bookName,url,publisher,author){
	// ISBN
	$("#isbn").val(isbn);
	$('p.isbn').text('');
	// 書籍名
	$("#bookName").val(bookName);
	// URL
	$("#url").val(url);
	// 出版社名
	$("#publisher").val(publisher);
	// 著者
	$("#author").val(author);
}

