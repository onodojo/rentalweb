/**
 * グループ管理画面
 */

//画面読み込み時処理(サイドメニュー対応)
$(window).load(function(){
	$(".menulink").removeClass("active");
	$(".menulink.group").addClass("active");
});

$(function() {
	PNotify.prototype.options.delay -= 6000;
	//TODO 場所/機器リストクリック時イベント
	$('.list-group').on('click', 'li', function(){
		var targetText = $(this).text();
		var targetVal = $(this).val();
		var index = $('li.list-group-item').index(this);
		var mode = "edit";
		var chkShelfId = $(this).attr("shelfid");
		
		if(targetVal == 1){
			$('#book').prop("checked",true);
			$('#tablet').prop("checked", false);
		}else if(targetVal == 2){
			$('#book').prop("checked",false);
			$('#tablet').prop("checked", true);
		}
		
		$('#editArea').val(targetText);
		$('.list-group').attr("index",index);
		
		$('li.list-group-item').css('background-color', '');
		$(this).css('background-color','#99ccff');
		changeMode(mode);
		if(chkShelfId  == ""){
			$('#btnDelete').show();
		}else{
			$('#btnDelete').hide();
		}
	});
	
	//追加ボタン押下時イベント
	$('#btnShelfAdd').on('click', function(){
	    $(this).before($('<div class="input-group" style="margin-bottom:5px;"/>')
	            .append($('<input type="text" class="form-control" name="shelf" class="editArea" data-id="">'))
	            .append($('<span class="input-group-btn"><button type="button" class="btn btn-danger btnShelfRemove">削除</button></span>')));
	    
//		var editText = $('#editArea').val();
//		var kind;
//		var html = "<li class='list-group-item' shelfid='' value=";
//		var addFlg = false;
//		
//		if($('#book').prop('checked')){
//			kind = $('#book').val();
//			addFlg = true;
//		}else if($('#tablet').prop('checked')){
//			kind = $('#tablet').val();
//			addFlg = true;
//		}
//		
//		if(editText != "" && addFlg){
//			html += kind + ">";
//			html += editText;
//			if(kind == 1){
//				html += "<i class='fa fa-book'></i></li>";
//			}else if(kind == 2){
//				html += "<i class='fa fa-tablet'></i></li>";
//			}
//			$(html).appendTo('.list-group');
//			resetEditArea();
//		}
		
        
	});
	
	$(document).on('click','button.btnShelfRemove',function(){
	    var $input = $(this).closest('div.input-group').find('input');
	    if($input.attr('data-id') !== ''){
	        if(confirm("「" + $input.val() + "」を削除します。¥nよろしいですか？")){
	            $(this).closest('div.input-group').hide();
	        }
	    }else{
	        $(this).closest('div.input-group').remove();
	    }
	});
	
	//反映ボタン押下時イベント
	$('button[name="edit"]').on('click',function(){
		var index = $('.list-group').attr("index");
		var editText = $('#editArea').val();
		var target = $('li.list-group-item').eq(index);
		var kind;
		var mode = "add";
		
		if($('#book').prop('checked')){
			kind = $('#book').val();
		}else if($('#tablet').prop('checked')){
			kind = $('#tablet').val();
		}
		
		if(editText != "" && kind != null){
			target.val(kind);
			target.text(editText);
			
			if(kind == 1){
				target.append("<i class='fa fa-book'></i>");
			}else if(kind == 2){
				target.append("<i class='fa fa-tablet'></i>");
			}
			$('li.list-group-item').css('background-color', '');
			resetEditArea();
			changeMode(mode);
		}
	});
	
	//キャンセルボタン押下時イベント
	$('button[name="cancel"]').on('click',function(){
		var mode = "add";
		
		$('li.list-group-item').css('background-color', '');
		resetEditArea();
		changeMode(mode);
	});
	
	$('#btnSave').on('click',function(){
		update();
	});
	
	//削除ボタン押下時イベント
	$('button[name="delete"]').on('click', function(){
		var index = $('.list-group').attr("index");
		var target = $('li.list-group-item').eq(index);
		var mode = "add";
		
		target.remove("li");
		resetEditArea();
		changeMode(mode);
	});
	
});

function update(){
	//TODO 更新権限のあるユーザか判定が必要
	var id = $('#groupId').val();
	var name = $('#groupName').val();
	var anounce = $('#announce').val();
	var group = {};
	var size = $('li.list-group-item').length;
	var shelfs = [];
	
	if(name != ""){
		group['id'] = id;
		group['name'] = name;
		group['anounce'] = anounce;
		
		$('input[name="shelf"]').each(function(){
		    var shelfList = {};
            shelfList['id'] = $(this).attr("data-id");
            shelfList['name'] = $(this).val();
            shelfList['remove'] = $(this).is(":hidden");
            shelfs.push(shelfList);
		});
		
//		for(var i=0; i < size; i++){
//			var shelfList = {};
//			shelfList['id'] = $('li.list-group-item').eq(i).attr("shelfid");
//			shelfList['name'] = $('li.list-group-item').eq(i).text();
//			//shelfList['kind'] = $('li.list-group-item').eq(i).val();
//			shelfs.push(shelfList);
//		}
		group['shelfList'] = shelfs;
		
		$.ajax({
	        type     : 'put',
	        url      : common.getAppRoot() + '/group/' + id,
	        data     : JSON.stringify(group),
	        contentType:"application/json",
	        dataType : 'json',
	        cache    : false,
	        beforeSend : function(jqXHR, settings) {
		        $('button[name="delete"]').attr('disabled', true);
	        }
	        }).done(function(data, status, jqXHR) {
	        	common.notifySuccess('保存しました');
	        }).fail(function(data, status, error) {
	        	common.notifyError(通信に失敗しました);
	        }).always(function(data, status, jqXHR){
	        	var mode = "add";
				$('button[name="delete"]').attr('disabled', false);
		    	$('li.list-group-item').css('background-color', '');
		    	resetEditArea();
		    	changeMode(mode);
	        });
        }else{
        	new PNotify({
	            title: "",
	            text: "グループ名を入力してください。",
	            nonblock: {
	                nonblock: true,
	                nonblock_opacity: .1
	            }
	        });
        }
}

//追加・編集項目のクリア
function resetEditArea(){
	$('#editArea').val("");
	$('#book').prop("checked",false);
	$('#tablet').prop("checked", false);
}

//ボタン表示・非表示制御
//addMode : 追加
//editMode : 反映・キャンセル・削除
function changeMode(mode){
	if(mode == "add"){
			$('#addMode').show();
			$('#editMode').hide();
			$('#btnDelete').hide();
	}else if(mode == "edit"){
			$('#addMode').hide();
			$('#editMode').show();
	}
};
