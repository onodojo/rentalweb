var oTable;
var targetRow;
var filter = 0;

//画面読み込み時処理(サイドメニュー対応)
$(window).load(function () {
    $(".menulink").removeClass("active");
    $(".menulink.apply").addClass("active");
});

$(function () {
    initTable(filter);

    // 追加ボタンクリック時
    $("#btnAdd").click(function () {
        $("#targetId").val("");
        $("#isbn").val("");
        $("#bookName").val("");
        $("#price").val("");
        $("#volume").val("");
        $("#reason").val("");
        $('input[name="buyer"]').val(['1']);

        $("#modalForm").find('p.text-danger').empty();

        $("#btnSave").removeAttr("disabled");
        $("#modalForm").modal('show');
    });

    // 削除ボタンクリック時
    $("#btnDelete").click(function () {
        if(!$('input.chkSelect:checked').get(0)){
            alert('１つ以上選択してください');
            return;
        }
        if (window.confirm("選択された申請を削除します。よろしいですか？")) {
            $('input.chkSelect:checked').each(function () {
                applyDelete($(this).closest('tr'));

                oTable.fnDeleteRow($(this).closest("tr"));
            });
        }
    });

    $('input[name="options"]').change(function(){
        filter = $(this).val();
        initTable(filter);
    });

    // 承認ボタン押下時
    $('#example1').on("click","a.btnApproval",function () {

        targetRow = $(this).closest('tr');
        var data = oTable.fnGetData(targetRow);

        $.ajax({
            url: common.getAppRoot() + '/apply/' + data.id,
            method: "GET",
            dataType: "json",
            success: function (data) {
                $('[data-name="isbn"]').text(data.data.isbn);
                $('[data-name="bookName"]').text(data.data.bookName);
                $('[data-name="type"]').text(data.data.bookType);
                $('[data-name="buyer"]').text(data.data.buyer);
                $('[data-name="price"]').text(data.data.price);
                $('[data-name="volume"]').text(data.data.volume);
                $('[data-name="reason"]').text(data.data.reason);
                $('[data-name="url"]').text(data.data.url);

                if(filter != 0 || !data.data.isAdmin){
                    $('#btnApproval').addClass('hide');
                    $('#btnReject').addClass('hide');
                }else{
                    $('#btnApproval').removeClass('hide');
                    $('#btnReject').removeClass('hide');
                }

                $('#modalApply').modal('show');
            },
            error:function(){
                alert("申請情報を取得できません。");
            }
        });
    });

    // ISBN自動取得
    $('#btnAutoAcqire').on('click', function () {
        clickAutoAcqire($('#isbn').val());
    });

    // 承認
    $('#btnApproval').click(function () {
        doApproval(common.getAppRoot() + "/apply/approval/", targetRow);
    });

    // 却下
    $('#btnReject').click(function () {
        doApproval(common.getAppRoot() + "/apply/cancel/", targetRow);
    });

});

/*
 * 申請処理
 */
function applyInsert() {

    var isbn = $('#isbn').val();
    var bookType;
    var bookName = $('#bookName').val();
    var price = $('#price').val();
    var volume = $('#volume').val();
    var url;
    var reason = $('#reason').val();
    var buyer = $('input[name="buyer"]:checked').val();

    if ($('#bookType').prop('checked') === true) {
        bookType = "2";
        url = $('#url').val();
    } else {
        bookType = "1";
    }

    //非同期通信
    $.ajax({
        type: 'post',
        url: common.getAppRoot() + '/apply',
        data: {
            isbn: isbn,
            bookName: bookName,
            bookType: bookType,
            price: price,
            buyer:buyer,
            url: url,
            volume: volume,
            reason: reason
        },
        dataType: 'json',
        cache: false,
        success: function (data, status, jqXHR) {
            common.notifySuccess('申請しました');
            $("#example1").DataTable().row.add(data).draw();
            $("#modalForm").modal('hide');
        },
        error: function (jqXHR, status, error) {
            
            Object.keys(jqXHR.responseJSON).forEach(function (p) {
                $('#' + p).next('p').text(jqXHR.responseJSON[p][0]);
            })

            common.notifyError('申請に失敗');
        }
    });
}

/*
 * 削除処理
 */
function applyDelete(row) {

    var data = oTable.fnGetData(row);

    //非同期通信
    $.ajax({
        type: 'delete',
        url: common.getAppRoot() + '/apply/delete/' + data.id,
        dataType: 'json',
        cache: false,
        success: function (data, status, jqXHR) {
            oTable.fnDeleteRow(row);
            common.notifySuccess('削除しました');
        },
        error: function (jqXHR, status, error) {
            common.notifyError('削除に失敗');
        }
    });
};

// 承認処理
var doApproval = function (uri, row) {
    var data = oTable.fnGetData(row);
    $.ajax({
        url: uri + data.id,
        method: "put",
        dataType: "json",
        success: function (data, status, jqXHR) {
            common.notifySuccess("正常に完了しました。");
            oTable.fnDeleteRow(row);
            $('#modalApply').modal('hide');
        },
        error: function (jqXHR, status, error) {
            common.notifyError("処理中にエラーが発生しました。");
        }
    });
};

// 書籍情報ISBN検索
var clickAutoAcqire = function (isbn) {
    //非同期通信
    $.ajax({
        type: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=' + isbn,
        cache: false,
        data_type: 'jsonp',
        success: function (data, status, jqXHR) {
            var items = data['items'];
            if (items) {
                var item = items[0];
                var volumeInfo = item['volumeInfo'];
                $("#bookName").val(volumeInfo['title']);
            } else {
                $('p.isbn').text('対象の書籍がみつかりません。');
                $("#bookName").val('');
            }
        },
        error: function (jqXHR, status, error) {
            common.notifyError('通信に失敗');
        }
    });
}

var initTable = function(filter){

    if(oTable){
        oTable.fnDestroy(false);
        oTable = undefined;
    }
    oTable = $("#example1").dataTable({
        "sAjaxSource":common.getAppRoot() + "/apply/list?f=" + filter,
        "oLanguage": common.datatableLabel,
        "aoColumnDefs": [
            {"mData":null,"sDefaultContent":"<input type='checkbox' class='chkSelect'>","aTargets":[0],"aClass":""},
            {"mData":"requestNo","aTargets":[1]},
            {"mData":"user","aTargets":[2]},
            {"mData":"applyDate","aTargets":[3]},
            {"mData":"isbn","aTargets":[4]},
            {"mData":"bookName","aTargets":[5]},
            {"mData":null,"sDefaultContent":"<a href='javascript:void(0)' class='btnApproval btn btn-link'>確認する</a>","aTargets":[6]},
            {"sWidth": "30px", "aTargets": [0, 6]},
            {"bSortable": false, "aTargets": [0, 6]}
        ]
    });
    oTable.fnSort([1, 'desc']);
}