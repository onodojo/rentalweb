/**
 * 共通処理
 */
var common = {};

common.datatableLabel = {
    "sProcessing": "処理中...",
    "sLengthMenu": "表示件数 _MENU_ 件",
    "sZeroRecords": "データはありません",
    "oPaginate": {
        "sNext": "次のページ",
        "sPrevious": "前のページ"
    },
    "sLoadingRecords": "読み込み中...",
    "sEmptyTable": "データはありません",
    "sInfo": "全_TOTAL_件中から_END_件を表示",
    "sInfoEmpty": "全0件中から0件を表示",
    "sInfoFiltered": "(全_MAX_件から検索)",
    "sSearch": "検索："
}

common.notifySuccess = function(msg){
    new PNotify({
        title: "Success",
        text: msg,
        nonblock: {
            nonblock: true,
            nonblock_opacity: .1
        },
        type: 'success'
    });
};

common.notifyError = function(msg){
    new PNotify({
        title: "Error",
        text: msg,
        nonblock: {
            nonblock: true,
            nonblock_opacity: .1
        },
        type: 'error'
    });
};

common.getAppRoot = function(){
    var path = './';
    var e = document.createElement('span');
    e.innerHTML = '<a href="' + path + '" />';
    url = e.firstChild.href;
    var p = url.split('/');
    return '/'+p[3];
}