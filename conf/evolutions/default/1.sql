# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table t_apply (
  id                        bigint not null,
  request_no                varchar(255),
  isbn                      varchar(255) not null,
  book_name                 varchar(255) not null,
  apply_date                timestamp not null,
  reason                    varchar(255) not null,
  volume                    integer not null,
  price                     integer not null,
  book_type                 varchar(255) not null,
  user_id                   bigint,
  group_id                  bigint,
  authorizer_id             bigint,
  authorize_date            timestamp,
  apply_state               varchar(255),
  url                       varchar(255),
  constraint pk_t_apply primary key (id))
;

create table m_book (
  id                        bigint not null,
  isbn                      varchar(255),
  name                      varchar(255),
  url                       varchar(255),
  publisher                 varchar(255),
  author                    varchar(255),
  recommend                 varchar(255),
  create_date               timestamp,
  shelf_id                  bigint,
  group_id                  bigint,
  constraint pk_m_book primary key (id))
;

create table m_group (
  id                        bigint not null,
  name                      varchar(255),
  anounce                   varchar(255),
  token                     varchar(255),
  new_arrival_span          integer,
  constraint pk_m_group primary key (id))
;

create table t_rental (
  id                        bigint not null,
  rental_date               timestamp,
  return_plan_date          timestamp,
  return_date               timestamp,
  book_id                   bigint,
  user_id                   bigint,
  group_id                  bigint,
  constraint pk_t_rental primary key (id))
;

create table m_shelf (
  id                        bigint not null,
  name                      varchar(255),
  kind                      integer,
  group_id                  bigint,
  constraint pk_m_shelf primary key (id))
;

create table m_user (
  id                        bigint not null,
  name                      varchar(255),
  password                  varchar(255),
  mailaddress               varchar(255),
  authority                 varchar(255),
  group_id                  bigint,
  constraint pk_m_user primary key (id))
;

create sequence t_apply_seq;

create sequence m_book_seq;

create sequence m_group_seq;

create sequence t_rental_seq;

create sequence m_shelf_seq;

create sequence m_user_seq;

alter table t_apply add constraint fk_t_apply_user_1 foreign key (user_id) references m_user (id);
create index ix_t_apply_user_1 on t_apply (user_id);
alter table t_apply add constraint fk_t_apply_group_2 foreign key (group_id) references m_group (id);
create index ix_t_apply_group_2 on t_apply (group_id);
alter table t_apply add constraint fk_t_apply_authorizer_3 foreign key (authorizer_id) references m_user (id);
create index ix_t_apply_authorizer_3 on t_apply (authorizer_id);
alter table m_book add constraint fk_m_book_shelf_4 foreign key (shelf_id) references m_shelf (id);
create index ix_m_book_shelf_4 on m_book (shelf_id);
alter table m_book add constraint fk_m_book_group_5 foreign key (group_id) references m_group (id);
create index ix_m_book_group_5 on m_book (group_id);
alter table t_rental add constraint fk_t_rental_book_6 foreign key (book_id) references m_book (id);
create index ix_t_rental_book_6 on t_rental (book_id);
alter table t_rental add constraint fk_t_rental_user_7 foreign key (user_id) references m_user (id);
create index ix_t_rental_user_7 on t_rental (user_id);
alter table t_rental add constraint fk_t_rental_group_8 foreign key (group_id) references m_group (id);
create index ix_t_rental_group_8 on t_rental (group_id);
alter table m_shelf add constraint fk_m_shelf_group_9 foreign key (group_id) references m_group (id);
create index ix_m_shelf_group_9 on m_shelf (group_id);
alter table m_user add constraint fk_m_user_group_10 foreign key (group_id) references m_group (id);
create index ix_m_user_group_10 on m_user (group_id);



# --- !Downs

drop table if exists t_apply cascade;

drop table if exists m_book cascade;

drop table if exists m_group cascade;

drop table if exists t_rental cascade;

drop table if exists m_shelf cascade;

drop table if exists m_user cascade;

drop sequence if exists t_apply_seq;

drop sequence if exists m_book_seq;

drop sequence if exists m_group_seq;

drop sequence if exists t_rental_seq;

drop sequence if exists m_shelf_seq;

drop sequence if exists m_user_seq;

