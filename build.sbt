name := "rentalweb"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  "org.postgresql" % "postgresql" % "9.3-1102-jdbc4",
  "org.webjars" % "jquery" % "2.1.4",
  "org.webjars" % "bootstrap" % "3.3.5",
  "com.typesafe.play.plugins" %% "play-plugins-mailer" % "2.3.1"
)

libraryDependencies += "com.orangesignal" % "orangesignal-csv" % "2.2.1"

play.Project.playJavaSettings
