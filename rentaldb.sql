
/* Drop Tables */

DROP TABLE IF EXISTS t_rental;
DROP TABLE IF EXISTS t_request;
DROP TABLE IF EXISTS m_user;
DROP TABLE IF EXISTS m_book;
DROP TABLE IF EXISTS m_group;




/* Create Tables */

CREATE TABLE m_user
(
	id serial NOT NULL,
	group_id int NOT NULL,
	name varchar(20) NOT NULL,
	email varchar(50) NOT NULL,
	-- 1:管理者
	-- 2:利用者
	authority char(1) NOT NULL,
	login_id varchar(20) NOT NULL,
	password varchar(20) NOT NULL,
	create_date timestamp,
	create_user int,
	update_date timestamp,
	update_user int,
	delete_date timestamp,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE m_group
(
	id serial NOT NULL,
	access_key varchar(20) NOT NULL UNIQUE,
	name varchar(20) NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE t_rental
(
	id serial NOT NULL,
	user_id int NOT NULL,
	book_id int NOT NULL,
	loan_date date NOT NULL,
	return_date date,
	return_plan_date date NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE m_book
(
	id serial NOT NULL,
	group_id int NOT NULL,
	isbn varchar(13) NOT NULL,
	name varchar(100) NOT NULL,
	publisher varchar(20) NOT NULL,
	author varchar(20) NOT NULL,
	recommend char(1) DEFAULT '0' NOT NULL,
	ebook char(1) DEFAULT '0' NOT NULL,
	url varchar(100),
	license int,
	create_date timestamp,
	create_user int,
	update_date timestamp,
	update_user int,
	delete_date timestamp,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE t_request
(
	id serial NOT NULL,
	user_id int NOT NULL,
	isbn varchar(13) NOT NULL,
	url varchar(100),
	amount int,
	price int,
	reason varchar(500) NOT NULL,
	reject_reason varchar(500),
	admin_id int,
	-- 0：承認済み
	-- 1：承認待ち
	-- 2：取下
	status char(1) NOT NULL,
	create_date timestamp,
	create_user int,
	update_date timestamp,
	update_user int,
	delete_date timestamp,
	PRIMARY KEY (id)
) WITHOUT OIDS;



/* Create Foreign Keys */

ALTER TABLE t_rental
	ADD FOREIGN KEY (user_id)
	REFERENCES m_user (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE t_request
	ADD FOREIGN KEY (user_id)
	REFERENCES m_user (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE m_user
	ADD FOREIGN KEY (group_id)
	REFERENCES m_group (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE m_book
	ADD FOREIGN KEY (group_id)
	REFERENCES m_group (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE t_rental
	ADD FOREIGN KEY (book_id)
	REFERENCES m_book (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



/* Comments */

COMMENT ON COLUMN m_user.authority IS '1:管理者
2:利用者';
COMMENT ON COLUMN t_request.status IS '0：承認済み
1：承認待ち
2：取下';



